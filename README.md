### Glinglin : Faire savoir que l'on sera là !

Permettre la visualisation et l'enregistrement rapide des participants aux événments d'un agenda à partir une page web.
Le tout stocké et synchroniser dans un agenda Nextcloud.

### Installation 

Pour installer rapidement Glinglin, voila les commandes.

```
git clone https://framagit.org/declic3000/glinglin.git
git submodule update --init --recursive
composer install
```



Pour faire fonctionner la synchronisation des tickets et agenda, il convient de renseigner le fichier
**app_connection.yaml** dans se trouve dans le dossier config, il liste des sources d'information externe.
Exemple de fichier config/app_connection.yaml

```
parameters:
  agenda :
    agenda_test:
      baseUri: 'http://nextcloud.localhost/remote.php/dav'
      userName: 'robert'
      password: 'TREFORT'
      calendriers: 
        'permanence' :
          title: 'Agenda des permanence'
          show_only : false
          read_only : false
        'vacances' :
          title: 'Information et periodes de vacances'
          show_only: true
          read_only: true
```
