const config = {
    nodeDir: './node_modules/',
    dir: './assets/',
    publicDir: './public/assets/',
    debug: false
};

const fichiers = {
    'bootstrap': [
        config.dir + 'scss/bootstrap.scss',
    ],
    'sass': [
        config.dir + 'scss/app.scss'

    ],
    'sass_print': [
        config.dir + 'scss/print.scss'
    ],
    'styles_print': [
        config.dir + 'css/styles_sass_print.css'
    ],
    'styles': [
        config.dir + 'css/fonts.css',
        //config.dir + 'font-svg/simplassofont.css',
        config.dir + 'css/bootstrap.css',
        config.nodeDir + 'pace-js/themes/pink/pace-theme-flash.css',
        config.nodeDir + 'datatables.net-bs4/css/dataTables.bootstrap4.css',
        config.nodeDir + 'datatables.net-keytable-bs4/css/keyTable.bootstrap4.css',
        config.nodeDir + 'datatables.net-responsive-bs4/css/responsive.bootstrap4.css',
        config.nodeDir + 'datatables.net-select-bs4/css/select.bootstrap4.css',


        //config.nodeDir + 'bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
        config.nodeDir + 'icheck/skins/flat/green.css',
        config.nodeDir + 'jquery-file-upload/css/uploadfile.css',
        config.nodeDir + 'pretty-checkbox/dist/pretty-checkbox.css',
        config.nodeDir + 'simplicite-bootstrap-datetimepicker/bootstrap5/css/bootstrap-datetimepicker.css',
        config.nodeDir + 'select2/dist/css/select2.css',
        config.nodeDir + 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.css',
        config.nodeDir + 'timepicker/jquery.timepicker.css',
        config.nodeDir + 'toastr/build/toastr.css',
        config.nodeDir + 'sweetalert2/dist/sweetalert2.css',
        config.dir + 'css/styles_sass.css'
    ],
    'scripts': [
        config.nodeDir + 'jquery/dist/jquery.min.js',
        config.nodeDir + 'jquery-form/src/jquery.form.js',
        config.nodeDir + 'jquery-extendext/jQuery.extendext.js',
        config.nodeDir + 'jquery-validation/dist/jquery.validate.js',
        config.nodeDir + 'moment/min/moment.min.js',
        config.nodeDir + 'moment/locale/fr.js',
        config.nodeDir + 'js-cookie/src/js.cookie.js',
        config.nodeDir + 'mustache/mustache.js',
        config.nodeDir + 'datatables.net/js/jquery.dataTables.js',
        config.nodeDir + 'datatables.net-bs4/js/dataTables.bootstrap4.js',
        config.nodeDir + 'datatables.net-plugins/sorting/date-eu.js',
        config.nodeDir + 'datatables.net-keytable/js/dataTables.keyTable.js',
        config.nodeDir + 'datatables.net-keytable-bs4/js/keyTable.bootstrap4.js',
        config.nodeDir + 'datatables.net-responsive/js/dataTables.responsive.js',
        config.nodeDir + 'datatables.net-responsive-bs4/js/responsive.bootstrap4.js',
        config.nodeDir + 'datatables.net-scroller/js/dataTables.scroller.js',
        config.nodeDir + 'datatables.net-select/js/dataTables.select.js',
        config.nodeDir + 'datatables.net-select-bs4/js/select.bootstrap4.js',
        config.nodeDir + 'sortablejs/Sortable.js',
        config.nodeDir + 'clipboard/dist/clipboard.js',
        config.nodeDir + 'toastr/build/toastr.min.js',
        config.nodeDir + 'sweetalert2/dist/sweetalert2.js',
        config.nodeDir + 'jquery-file-upload/js/jquery.uploadfile.js',
        config.dir + 'js/flagadajs_vars.js',
        config.dir + 'flagadajs/**/*.js',
        config.dir + 'js/page/*.js',
        config.dir + 'js/templates.js',
        config.dir + 'js/barre_filtre.js',
        config.dir + 'js/all.fa-min.js',
        config.dir + 'js/scripts.js'
    ],
    'scripts_bas_de_page': [
        config.nodeDir + 'fastclick/lib/fastclick.js',
        config.nodeDir + 'pace-js/pace.js',
        config.nodeDir + 'typeahead.js/dist/bloodhound.js',
        config.nodeDir + 'typeahead.js/dist/typeahead.jquery.js',
        config.nodeDir + 'jquery-slimscroll/jquery.slimscroll.js',
        config.nodeDir + '@popperjs/core/dist/umd/popper.js',
        config.nodeDir + 'bootstrap/dist/js/bootstrap.bundle.js',
        config.nodeDir + 'bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
        config.nodeDir + 'bootstrap-select/dist/js/bootstrap-select.js',
        config.nodeDir + 'bootstrap-select/dist/js/i18n/defaults-fr_FR.js',
        config.nodeDir + 'bootstrap-validator/js/validator.js',
        config.nodeDir + 'jquery.inputmask/dist/inputmask/inputmask.js',
        config.nodeDir + 'jquery.inputmask/dist/inputmask/jquery.inputmask.js',
        config.nodeDir + 'jquery.inputmask/dist/inputmask/inputmask.date.extensions.js',
        config.nodeDir + 'simplicite-bootstrap-datetimepicker/bootstrap5/js/bootstrap-datetimepicker.js',
        config.nodeDir + 'simplicite-bootstrap-datetimepicker/bootstrap5/js/locales/bootstrap-datetimepicker.fr.js',
        config.nodeDir + 'timepicker/jquery.timepicker.js',
        config.nodeDir + 'select2/dist/js/select2.full.js',
        config.nodeDir + 'select2/dist/js/i18n/fr.js'

    ],
    'scripts_webpack': [
        config.dir + 'js/fullcalendar.js'
    ]

};


const icones_fa = {
    fal: [],
    far: ['copy', 'print', 'files', 'file', 'clock', 'newspaper', 'envelope', 'times-circle', 'trash-alt', 'calendar', 'times-circle'],
    fas: ['mobile-alt','question-circle', 'certificate', 'tools','paint-brush','candy-cane','comment','link','mouse-pointer','seedling', 'piggy-bank', 'wallet',    'directions',    'unlock-alt',    'map-signs', 'i-cursor','dungeon','building',   'code-branch','toolbox', 'grin-beam','sticky-note','ban', 'check-circle', 'redo', 'vote-yea', 'chevron-down', 'money-bill-wave', 'arrow-circle-right', 'arrow-down', 'arrow-up', 'fa-calendar-check', 'arrow-circle-right', 'mail', 'star', 'table', 'copy', 'tag', 'tags', 'backward', 'flag', 'unlink', 'save', 'lock', 'caret-square-down', 'check-square', 'times', 'exclamation', 'plus', 'sort', 'sort-up', 'sort-down', 'sort-amount-up', 'sort-amount-down', 'plus-circle', 'search', 'minus', 'minus-circle', 'export', 'crown', 'check-circle', 'asterisk', 'exclamation-triangle', 'info-circle', 'th-list', 'file-import', 'file-export', 'file-download', 'tag', 'calendar', 'close', 'spinner', 'pen-square', 'question-circle', 'street-view', 'map-marker', 'newspaper', 'eye', 'backward', 'envelope', 'clock', 'print', 'euro-sign', 'circle', 'files', 'file', 'arrow-circle-right', 'arrow-circle-down', 'inbox', 'home', 'check', 'chevron-left', 'chevron-right', 'send', 'stethoscope', 'sign-in-alt', 'sign-out-alt', 'chart-bar', 'bars', 'rss', 'angle-double-down', 'home', 'paper-plane', 'frown', 'smile', 'hand-peace', 'file-pdf', 'file-excel', 'file-csv', 'card', 'filter', 'id-card', 'cog', 'cogs', 'life-ring', 'database', 'bell', 'phone', 'mobile-alt', 'gift', 'list', 'rocket', 'info-circle', 'info-circle', 'paint-brush', 'paint-brush', 'book', 'bookmark', 'globe', 'pencil-alt', 'trash-alt', 'compress', 'user', 'users', 'plus-circle'],
    fab: ['php','font-awesome','linux', 'black-tie', 'mastodon']
};




    module.exports = [config,fichiers,icones_fa];