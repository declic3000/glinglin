let data_notifications =[];

$.fn.extend({
    'initialisation_js': initialisation_js,
    'initialisation_form_autres_composants': initialisation_form_autres_composants
});

$(document).ready(function(){

    //initialisation_notification(data_notifications);
    initialisation_btn_entete();
    initialisation_rafraichir_agenda();
    $(this).initialisation_js();
});

function init_js(obj){
    $(obj).initialisation_js();
}

function initialisation_js(){
    $(this).initialisation_ajax();
    $(this).initialisation_depliable();
    $(this).initialisation_voir_plus();
    $(this).initialisation_sortable();
    $(this).initialisation_composant_forms();
    $(this).initialisation_modal();
    //$(this).initialiseXeditable();
    $(this).initialisation_ajax();
    $(this).initialisation_clipboard();
    $(this).initialisation_filtre();
    const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
    const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
}


function initialisation_btn_entete(){

    $('#entete #btn_minicalendrier').on('click',function(){
        if ($(this).hasClass('active')){
            $(this).removeClass('active');
            $('#calendriers').slideUp('fast');
            Cookies.set('minicalendrier', 0)
        }
        else{
            $(this).addClass('active');
            $('#calendriers').slideDown();
            Cookies.set('minicalendrier', 1)
        }
    });

    let aff_calendrier =  Cookies.get('minicalendrier');
    if (aff_calendrier==1){
        $('#calendriers').slideDown();
        $('#entete #btn_minicalendrier').addClass('active');
    }

}

function initialisation_rafraichir_agenda(){

    let temps_serveur = parseInt($('#evts').data('refresh'))
    let temps_client = (new Date().getTime())/1000;
    let diff = (temps_client - temps_serveur)
    if (diff > 600) {
        location.reload();
    }
    setInterval(testNouveaute,1000*30)
}


function testNouveaute(){
    let url_agenda_nouveaute = glinglin.urls.agenda_nouveaute;
    $.getJSON(url_agenda_nouveaute,function(data){
        let temps_serveur = parseInt($('#evts').data('refresh'));
        let diff = (data.date - temps_serveur)
        if (diff > 0) {
            $('#alerte_rafraichir').slideDown('fast');
        }
    });
    return false;
}


function initialisation_form_autres_composants(){

    $(this).initialisation_champs_autocomplete_select2();
    $(this).initialisation_champs_autocomplete_automatique();
}


