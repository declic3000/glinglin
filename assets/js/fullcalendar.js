import glinglin from 'glinglin';
import {Calendar} from "fullcalendar";
import dayGridPlugin from '@fullcalendar/daygrid'
import listPlugin from '@fullcalendar/list';
import frLocale from '@fullcalendar/core/locales/fr';
import timeGridPlugin from '@fullcalendar/timegrid';
var temporisation_deplacement = false;
var temporisation_placement = false;
let deployer_timeout;
let remballer_timeout;
let grille_indispo_active=false;

function agenda_rafraichir(){
    calendar.refetchEvents();
}
window.agenda_rafraichir = agenda_rafraichir;

document.addEventListener('DOMContentLoaded', function() {

    let largeur = window.innerWidth;
    let calendarEl = document.getElementById('calendar');
    if (calendarEl == undefined){
        return true;
    }
    let consultation= $('#calendar').data("consultation");
    if (largeur < 992 ){
        let url='?mode_affichage=liste';
        if (consultation){
            url = 'public'+url;
        }
        document.location='/'+url;
    }

    let options={
        editable: false,
        eventDurationEditable: false,
        eventOverlap: false,
        droppable: false,
        aspectRatio: 1.8,
        locales: [  frLocale ],
        locale: 'fr',
        firstDay:1,
        themeSystem: 'bootstrap5',
        events: {
            'url': glinglin.urls.calendrier_evts,
            'extraParams': function () {
                let args = $('#calendar').data("filtres");
                args['consultation'] = $('#calendar').data("consultation");
                return args;
            },
            'failure': function () {
                console.log('Aucun evt');
            },
            'success': function () {
                console.log("chargement evts ok");
            }
        },
        eventsSet: function (arg) {
            $("#calendar .fc-event").each(function(){
                $(this).data('filtre-valeur-cat',$('.filtre_ob',this).data('filtre-valeur-cat'));
                $(this).data('filtre-valeur-lieu',$('.filtre_ob',this).data('filtre-valeur-lieu'));
                $(this).addClass('filtre_ob');
                $(this).initialisation_js();
            });
        }
    }

    let options_special={};

        options_special={
            plugins: [dayGridPlugin,listPlugin,timeGridPlugin ],
            initialView: 'timeGridWeek',
            //initialView: 'dayGridMonth',
            navLinks: false,
            headerToolbar: {
                left: 'prev,next',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek'
            },
            allDaySlot: false,
            slotMinTime: '08:00:00',
            slotMaxTime: '23:00:00',
            businessHours: {
                daysOfWeek: [1, 2, 3, 4, 5, 6],
                startTime: '10:00',
                endTime: '21:00', // an end time (6pm in this example)
            },
            eventContent: function (arg) {
                let args={
                    'id': arg.event.id,
                    'titre': arg.event.title,
                    'timeText': arg.timeText
                };
                let info_suppl = arg.event.extendedProps;
                if (info_suppl !== undefined ) {
                    let categories = [];
                    if (info_suppl.categories){
                        categories = info_suppl.categories;
                    }
                    $.extend(args,{
                        'lieu': info_suppl.lieu,
                        'tab_categories': categories.join('|'),
                        'couleur_bg': info_suppl.couleur,
                        'participants': info_suppl.participant,
                        'nb_personne': info_suppl.nb_personne,
                        'canceled': (info_suppl.statut==='CANCELLED'),
                        'besoin': (info_suppl.nb_personne>info_suppl.participant.length),
                        'public': consultation
                    })
                }
                args['consultation'] = $('#calendar').data("consultation");
                let html = Mustache.render(window['templates']['evt.html'](), args);
                return {'html': html};
            },


            eventClick: function (info) {
                info.jsEvent.preventDefault();
               $('.action a',$(info.el)).trigger('click');
               return false;
            }

        }


        $.extend(options,options_special);
        let calendar = new Calendar(calendarEl, options);
        calendar.render();



});




