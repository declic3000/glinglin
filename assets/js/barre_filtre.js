let timer_recherche = 0;

$(document).on("rafraichirDatatable", rafraichirDatatableHandler);

function rafraichirDatatableHandler(e) {
    relanceRecherche(type,options);
    return false;
}

function activeInteractionTriFiltre(type,options){
    selecteur_barre_filtre = $('#filtres');

    selecteur_haut = $('.haut_de_table');

    $('#filtre_recherche', selecteur_barre_filtre).keyup(function () {

        if (timer_recherche)
            clearTimeout(timer_recherche);
        timer_recherche = setTimeout(function () {
            relanceRecherche(type,options);
        }, 300);

    }).bind('blur',function () {

    });




    $('div.trier_par_selection a', selecteur_haut).click(function () {
        $.event.trigger({type: "rafraichirDatatable" });
        $('div.trier_par_selection', selecteur_haut).dropdown('toggle');
        return false;
    });



    $('select', selecteur_barre_filtre).change(function () {
            if ($(this).data().data){
                relanceRecherche(type,options);
                return false;
            }
        }
    );


    $('select.select2', selecteur_barre_filtre).select2({
        theme : "bootstrap",
        allowClear : true,
        minimumResultsForSearch : 20,
        dropdownAutoWidth : true
    });

    $('.select2', selecteur_barre_filtre).on('select2:select', function (e) {
            relanceRecherche(type,options);
            return false;

    });
    $('select.select2_simple', selecteur_barre_filtre).select2({
        theme : "bootstrap",
        allowClear : true,
        minimumResultsForSearch : Infinity,
        dropdownAutoWidth : true
    });
    $('select.select2_simple', selecteur_barre_filtre).on('select2:select', function (e) {
        relanceRecherche(type,options);
        return false;
    });


    $('input[type=checkbox]', selecteur_barre_filtre).change(function () {
            relanceRecherche(type,options);
            return false;
        }
    ).on('ifClicked',function(){
        relanceRecherche(type,options);
        return false;
    });


    $('.panneau input[type=text]', selecteur_barre_filtre).change(function () {
            relanceRecherche(type,options);
            return false;
        }
    );

    $('#filtrer_plus', selecteur_barre_filtre).click(function () {
        $('#filtres_secondaires', selecteur_barre_filtre).removeClass('hide');
    });


  /* $('.panneau .menu li',selecteur_barre_filtre).click(function () {
        cible = $(this).attr('data-href');
        if ($(this).hasClass('on')) {
            cible = $(this).attr('data-href');
            $('.panneau').hide();
            $('#panneau_' + cible).slideUp();
            $(this).removeClass('on');
            relanceRecherche(type,options);
        } else {
            $('.panneau').hide();
            $('.menu li').removeClass('on');
            $(this).addClass('on');
            $('#panneau_' + cible).stop().slideDown();
        }
    });*/

    $('.panneau li',selecteur_barre_filtre).click(function () {
    	
    	
        if ($(this).hasClass('on')) {
        	
            $(this).removeClass('on');
        } else {
        	
            $(this).addClass('on');
        }
    });

    $('.panneau li',selecteur_barre_filtre).dblclick(function () {
        if (!$(this).hasClass('on')) {
            $(this).addClass('on');
        }
        $('.menu li.on',selecteur_barre_filtre).trigger('click');
    });

    $('.panneau .btn-ok',selecteur_barre_filtre).click(function () {
        $('.menu li.on',selecteur_barre_filtre).trigger('click');
    });




    $('input.icheck_ind[type=checkbox]',selecteur_barre_filtre).each(function(){

        label = $(this).parent('label');
        $(label).attr('data-key',$(this).attr('name'));
        $(label).addClass('icheck_ind').click( function(event){

                if($(this).hasClass('on')) {
                    $(this).removeClass('on');
                    $(this).addClass('off');
                    $(this).attr('data-value','non');

                }
                else
                {
                    if( !$(this).hasClass('off')) {
                        $(this).addClass('on');
                        $(this).attr('data-value','oui');
                    }
                    else{
                        $(this).removeClass('off');
                        $(this).attr('data-value','');
                    }
                }
            relanceRecherche(type,options);
            return false;
            });
        $(this).remove();
        });




    $('.btn-raz',selecteur_haut).click(function(){
        cadre = $(this).parent();
        $('li.on',cadre).removeClass('on');
        $('div.on',cadre).removeClass('on');
        relanceRecherche(type,options);
    });
    $('.btn-raz-total',selecteur_haut).click(function(){
        $('.panneau li.on').removeClass('on');
        $('div.on').removeClass('on');
          relanceRecherche(type,options);
    });


}





function initialiser_menu_filtre() {

    $('.menu > li').click(function() {
        cible = $(this).attr('data-href');
        if ($(this).hasClass('on')) {
            cible = $(this).attr('data-href');
            $('.panneau').hide();
            $('#panneau_' + cible).slideUp();
            $(this).removeClass('on');
            $('#filtre_recherche').trigger('keyup');
        } else {
            $('.panneau').hide();
            $('.menu li').removeClass('on');
            $(this).addClass('on');
            $('#panneau_' + cible).stop().slideDown();
        }
    });



    $('.panneau .btn-ok').click(function() {

        $('.menu li.on').trigger('click');
    });
}






function ajouter_filtre_actif(key, valeur, selecteur_filtre) {

    filtres = $('#filtres .panneau');


    if (selecteur_filtre) {

        libelle_key = $(selecteur_filtre).prev('label').text();
        if (!libelle_key)
            libelle_key = key;
        if ($(selecteur_filtre).is("select")) {
            var libelle_valeur = [];
            $(selecteur_filtre).find(':selected').each(function () {
                if($(this).data().data){
                    libelle_valeur.push($(this).data().data.element.text);
                }else {
                    libelle_valeur.push($(this).text());
                }
            });
            if (libelle_valeur.length > 0)
                libelle_valeur = libelle_valeur.join(', ');
        }
        else if ($(selecteur_filtre).attr('type') === 'hidden') {
            libelle_key = $(selecteur_filtre).attr('data-name');
            libelle_valeur = $(selecteur_filtre).attr('data-value');
        }
        else {
            libelle_valeur = valeur;
        }
    }
    else {

        champs = $('li.on[data-key=' + key + '][data-value=' + valeur + ']', filtres);
        if (champs.length>0){
            libelle_key = $(champs).parents('ul').prev('label').text();
            libelle_valeur = $(champs).html();
        } else {
            champs = $('li.off[data-key=' + key.substr(6) + '][data-value=' + valeur + ']', filtres);
            if (champs.length>0){
                libelle_key = $(champs).parents('ul').prev('label').text();
                libelle_valeur = $(champs).html();
            }
            else {
                champs = $('label.icheck_ind[data-key=' + key + '][data-value=' + valeur + ']', filtres);
                libelle_key = key;
                libelle_valeur = valeur;
            }
        }

    }

    if (libelle_valeur) {
    	
        element = $('<li class="list-inline-item" data-key="' + key + '" data-value="' + valeur + '"><div class="small">' + libelle_key + '</div>' + libelle_valeur + '</li>');
        if (selecteur_filtre) {
            $('#filtre_en_cours ul').append(element);
            if ($(selecteur_filtre).is("select")) {
                $(element).click(function () {
                    $(selecteur_filtre).val(null).trigger('change');
                    $('#filtre_recherche').trigger('keyup');
                });
            }
            else {
            	
                if ($(selecteur_filtre).attr('type') === 'hidden') {
                    $(element).click(function () {
                        $(selecteur_filtre).removeAttr('value');
                        $(selecteur_filtre).attr('data-value','');
                        $('#filtre_recherche').trigger('keyup');
                    });
                }
                else {
                    $(element).click(function () {
                    	$(selecteur_filtre).val('');
                        if (selecteur_filtre==='input#filtre_recherche'){
                        	$('#filtre_recherche').trigger('keyup');
                        	}
                        else{
                        	$(selecteur_filtre).trigger('change');}
                    });
                }
            }
        }
        else {
            $('#filtre_en_cours ul').append(element);
            $(element).click(function () {
                key = $(this).attr('data-key');
                $('li[data-key=' + key + '][data-value=' + valeur + ']', filtres).removeClass('on');
                $('label.icheck_ind[data-key=' + key + '][data-value=' + valeur + ']', filtres).removeClass('on').attr('data-value','');
                $('#filtre_recherche').trigger('keyup');
            });
        }
    }
}




function recap_filtre(filtres) {
    args = {};
    type = [];
    var ok = false;
    $('#filtre_en_cours ul').html('');

    for (i = 0; i < filtres.length; i++) {
        selecteur_filtre = $('.filtres select[name=' + filtres[i] + ']');
        if (selecteur_filtre.length == 0)
            selecteur_filtre = $('.filtres select[name=' + filtres[i] + '\\[\\]]');
        if (selecteur_filtre.length > 0) {
            valeur = $(selecteur_filtre).val();
            if (valeur && valeur != '') {
                ajouter_filtre_actif(filtres[i], valeur, selecteur_filtre);
                args[filtres[i]] = valeur;
                ok = true;
            }
        } else {
            selecteur_filtre = $('.filtres input[type=checkbox][name=' + filtres[i] + ']');
            if (selecteur_filtre.length > 0) {
                selecteur_filtre = $('.filtres input[name=' + filtres[i] + ']:checked');
                if (selecteur_filtre.length == 0)
                    selecteur_filtre = $('.filtres input[name=' + filtres[i] + '\\[\\]]:checked');
                if (selecteur_filtre.length > 0) {
                    valeur = $(selecteur_filtre).val();
                    if (valeur) {
                        ajouter_filtre_actif(filtres[i], valeur, selecteur_filtre);
                        args[filtres[i]] = valeur;
                        ok = true;
                    }
                }
            }
            else {
                if (filtres[i] === 'search'){
                    selecteur_filtre = $('.filtres input[type=text][name=' + filtres[i] + '\\[value\\]]');
                }
                else {
                    selecteur_filtre = $('.filtres input[type=text][name=' + filtres[i] + ']');
                }
                if (selecteur_filtre.length > 0) {
                    if (filtres[i] === 'search') {
                        recherche = $('input#filtre_recherche').val();
                        if (recherche) {
                            args['search'] = {regex: false, value: recherche};
                            ajouter_filtre_actif('recherche', recherche, 'input#filtre_recherche');
                            ok = true;
                        }
                    }
                    else {
                        valeur = $(selecteur_filtre).val();
                        if (valeur) {
                            ajouter_filtre_actif(filtres[i], valeur, selecteur_filtre);
                            args[filtres[i]] = valeur;
                            ok = true;
                        }
                    }
                }
                else {
                    selecteur_filtre = $('.filtres input[type=hidden][name=' + filtres[i] + ']');

                    if (selecteur_filtre.length > 0) {
                        valeur = $(selecteur_filtre).val();
                        ajouter_filtre_actif(filtres[i], valeur, selecteur_filtre);
                        args[filtres[i]] = valeur;
                        ok = true;
                    }
                    else{
                        //console.log('filtre introuvable : '+filtres[i]);
                    }

                }
            }
        }
    }


    var panneau = $('#filtres .panneau');
    $('li.on', panneau).each(function () {
        key = $(this).attr('data-key');
        value = $(this).attr('data-value');
        
        ajouter_filtre_actif(key, value);
        if (args[key])
            args[key].push(value);
        else
            args[key] = [value];
        ok = true;
    });

    $('li.off', panneau).each(function () {
        key = 'not_in_'.$(this).attr('data-key');
        value = $(this).attr('data-value');

        ajouter_filtre_actif(key, value);
        if (args[key])
            args[key].push(value);
        else
            args[key] = [value];
        ok = true;
    });

    $('label.icheck_ind', panneau).each(function () {
        key = $(this).attr('data-key');
        value = $(this).attr('data-value');

        if (value !== undefined && value !== '' ){
            ajouter_filtre_actif(key, value);
        }
        args[key] = value;
        ok = true;
    });



    if (ok) {
    	$('#filtre_en_cours .titre_ko').hide();
        
    } else {
    	$('#filtre_en_cours .titre_ko').show();
    }

    return args;
}
