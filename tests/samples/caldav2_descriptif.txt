Chaque mercredi soir\, l'association propose une rencontre pour
  partager des connaissances\, des savoir-faire\, des questions autour de l
 'utilisation des logiciels libres\, que ce soit à propos du système d'ex
 ploitation Linux\, des applications libres ou des services en ligne libres
 .\n\nC'est l'occasion aussi de mettre en avant l'action des associations f
 édératrices telles que l'April ou Framasoft\, dont nous sommes adhérent
 s et dont nous soutenons les initiatives trés cool.