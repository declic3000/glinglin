<?php

namespace App\Action;

use App\Entity\Caracteristique;
use Declic3000\Pelican\Action\Action;

class CaracteristiqueAction extends Action
{
    function form_save_after_flush(Caracteristique $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

    function deleteCplt(){
        $this->sac->clear();
        $this->sac->initSac(true);
    }
}