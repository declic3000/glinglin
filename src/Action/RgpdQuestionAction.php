<?php

namespace App\Action;

use App\Entity\RgpdQuestion;
use Declic3000\Pelican\Action\Action;


class RgpdQuestionAction extends Action
{

    function form_save_after_flush(RgpdQuestion $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

    function deleteCplt(){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

}