<?php

namespace App\Action;


use App\Entity\Categorie;
use Declic3000\Pelican\Action\Action;


class CategorieAction extends Action
{
    function form_save_after_flush(Categorie $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

    function deleteCplt(){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

}