<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Categorie;
use App\Entity\Config;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Enum\Classification;
use App\Enum\Status;
use App\Form\AddEvtsType;
use App\Form\EvenementLotEvtType;
use App\Form\EvenementLotType;
use App\Form\EvtType;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sabre\VObject;

#[Route(path: '/admin/evt')]

class EvenementController extends Controller
{


    #[Route(path: '/', name: 'evenement')]
    public function evenement(CaldavService $caldavService): Response
    {
        $args_twig=[];
        return $this->render('evenement/index.html.twig',$args_twig);
    }




    #[Route(path: '/agenda/add_evt', name: 'agenda_add_evt')]
    public function agenda_add_evt(CaldavService $caldavService): Response
    {
        $args_rep=[];
        $date_du_jour = new \DateTime();
        $datas = [
            'date' => $date_du_jour,
            'status'=> 1,
            'disponibilite'=> 1,
        ];
        $args = [];
        $form = $this->generateForm('addEvt',EvtType::class,$datas,[],false,$args);
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() ){
            //$tab_mot = $this->requete->getRequest()->request->all('addEvt');
            if ( $form->isValid()) {
                $datas = $form->getData();
                $tab_categorie=table_simplifier($this->sac->tab('categorie'));
                $tab_cat_select2 = json_decode((string) $form->get('categorie_add')->getData(),true);
                $tab_cat_new =[];
                if (!empty($tab_cat_select2)) {
                    foreach ($tab_cat_select2 as $new_cat) {
                        if (!$new_cat['selected'] &&
                            $new_cat['id'] === $new_cat['text']) {
                            if (!in_array($new_cat['text'], $tab_categorie)) {
                                $categorie = new Categorie();

                                $categorie->setNom($new_cat['text']);
                                $this->em->persist($categorie);
                                $this->em->flush();
                                $tab_cat_new[] = $categorie->getNom();
                                $this->sac->initSac(true);
                            }
                        }
                    }
                }
                $tab_lieu = table_simplifier($this->sac->tab('lieu'));
                $tab_lieu_new = json_decode((string) $form->get('lieu_add')->getData(),true);
                if (!empty($tab_lieu_new)){
                    foreach($tab_lieu_new as $new_lieu){
                        if (!$new_lieu['selected'] &&
                            $new_lieu['id'] ===$new_lieu['text'] ){
                            if (!in_array($new_lieu['text'],$tab_lieu)){
                                $lieu = new Lieu();
                                $lieu->setNom($new_lieu['text']);
                                $this->em->persist($lieu);
                                $this->em->flush();
                                $datas['lieu']= $lieu->getNom();
                                $this->sac->initSac(true);
                            }
                        }
                    }
                }
                $date = $datas['date'];
                $date_debut = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$datas['heure_debut'].':00');
                $date_fin = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$datas['heure_fin'].':00');
                $options = [
                    'organisateur'=>$datas['organisateur']??'',
                    'organisateur_email'=>$datas['organisateur_email']??'',
                    'confidentialite'=> $datas['confidentialite']??'2',
                    'categorie'=> array_merge($datas['categorie']??[],$tab_cat_new),
                    'couleur'=>$datas['couleur']??'',
                    'status'=>$datas['status']??'confirmed',
                ];

                $description = $caldavService->construireDescription($datas['observation'],$datas['nb_personne'],$datas['presentation']);

                $caldavService->addEvt(
                    $datas['serveur'],
                    $datas['calendrier'],
                    $datas['titre'],
                    $description,
                    $datas['lieu'] ?? '???',
                    $date_debut,
                    $date_fin,
                    $options,
                    true);

                $args_rep['ok'] = true;
                $this->addFlash('success','Evenement ajouté');
                $args_rep['url_redirect']=$this->generateUrl('agenda');
            }

        }
        return $this->reponse_formulaire($form, $args_rep,'evenement/add_evt.html.twig');
    }



    #[Route(path: '/agenda/{id}/edit_evt', name: 'agenda_edit_evt')]
    public function agenda_edit_evt(CaldavService $caldavService,int $id): Response
    {
        $args_rep=[];
        $date_du_jour = new \DateTime();

        $chargeur = new Chargeur($this->em);
        $objet_data = $chargeur->charger_objet('cal',$id);
        $datas = $objet_data->toExport();

        $args = [];
        $form = $this->generateForm('editEvt',EvtType::class,$datas,[],false,$args);
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() ){
            //$tab_mot = $this->requete->getRequest()->request->all('addEvt');
            if ( $form->isValid()) {
                $datas = $form->getData();
                $tab_categorie=table_simplifier($this->sac->tab('categorie'));
                $tab_cat_select2 = json_decode((string) $form->get('categorie_add')->getData(),true);
                $tab_cat_new =[];
                if (!empty($tab_cat_select2)) {
                    foreach ($tab_cat_select2 as $new_cat) {
                        if (!$new_cat['selected'] &&
                            $new_cat['id'] === $new_cat['text']) {
                            if (!in_array($new_cat['text'], $tab_categorie)) {
                                $categorie = new Categorie();

                                $categorie->setNom($new_cat['text']);
                                $this->em->persist($categorie);
                                $this->em->flush();
                                $tab_cat_new[] = $categorie->getNom();
                                $this->sac->initSac(true);
                            }
                        }
                    }
                }
                $tab_lieu = table_simplifier($this->sac->tab('lieu'));
                $tab_lieu_new = json_decode((string) $form->get('lieu_add')->getData(),true);
                if (!empty($tab_lieu_new)){
                    foreach($tab_lieu_new as $new_lieu){
                        if (!$new_lieu['selected'] &&
                            $new_lieu['id'] ===$new_lieu['text'] ){
                            if (!in_array($new_lieu['text'],$tab_lieu)){
                                $lieu = new Lieu();
                                $lieu->setNom($new_lieu['text']);
                                $this->em->persist($lieu);
                                $this->em->flush();
                                $datas['lieu']= $lieu->getNom();
                                $this->sac->initSac(true);
                            }
                        }
                    }
                }

                $caldavService->majCal($objet_data,$datas);
                $date = $datas['date'];
                $datas['date_debut'] = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$datas['heure_debut'].':00');
                $datas['date_fin'] = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$datas['heure_fin'].':00');
                $objet_data = $caldavService->majCal($objet_data,$datas);
                $this->em->persist($objet_data);
                $this->em->flush();
                $ok = $caldavService->syncEvt($objet_data,true);
                $args_rep['ok'] = $ok;
                if ($ok){
                    $this->addFlash('success','Evenement modifié');
                }
                else{
                    $args_rep['erreur'] = false;
                    $this->addFlash('danger','Erreur!');
                }
                $args_rep['url_redirect']=$this->generateUrl('agenda');

            }

        }
        return $this->reponse_formulaire($form, $args_rep,'evenement/edit_evt.html.twig');
    }



    #[Route(path: '/agenda/{id}/del_evt', name: 'agenda_delete_evt')]
    public function agenda_delete_evt(CaldavService $caldavService,int $id): Response
    {
        $objet='cal';
        $chargeur = new Chargeur($this->em);
        $objet_data = $chargeur->charger_objet($objet,$id);
        $descr_objet = $this->sac->descr($objet);
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_SUP')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $data = ['ok' => false, 'message' => "Erreur dans la suppression"];

        if ($this->isCsrfTokenValid('delete-' . $objet . '-' . $objet_data->getPrimaryKey(), $this->requete->get('_token'))) {

            $em = $this->em;
            $id = $objet_data->getPrimaryKey();
            try {
                $em->remove($objet_data);
                $em->flush();
     } catch (\Exception $exception) {
                $data['message'] .= $exception->getMessage();
                return $this->json($data);
            }
            if (empty($data['redirect'])) {
                $data['redirect'] = $this->generateUrl('accueil');
            }
            $this->sync($caldavService);
            $data['message'] = 'Le ' . $objet . ' a été supprimé';
            $data['ok'] = true;
            $this->log('DEL', $objet, $id);
        }


        return $this->json($data);
    }


    #[Route(path: '/agenda/add_evts', name: 'agenda_add_evts')]
    public function agenda_add_evts(CaldavService $caldavService): Response
    {

        $args_rep=[];
        $datas=[];
        $args=[];
        $date_du_jour = new \DateTime();
        $datas['annee']=$date_du_jour->format('Y');
        $datas['semaine']=$date_du_jour->format('W');
        $tab_serveur = $caldavService->getListeServeur(true);
        if (count($tab_serveur)==1){
            $datas['serveur']=array_pop($tab_serveur);
        }

        $tab_serveur = $caldavService->getListeServeurCalendrier(true);
        $tab_calendrier = [];
        foreach($tab_serveur as $serveur => $t_calendrier){
            foreach($t_calendrier as $calendrier=>$calendrier_info){
                $tab_calendrier["$calendrier"] = $calendrier;
            }
        }
        if (count($tab_calendrier)==1){
            $datas['calendrier']=array_pop($tab_calendrier);
        }
        $form = $this->generateForm('permanenceEvt',AddEvtsType::class,$datas,[],false,$args);
        $tab_cat = table_simplifier($this->tab('categorie'));
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();

            $tab_permanence  = $this->sac->conf('permanences');
            $indice = $datas['indice_lot'];
            foreach($tab_permanence[$indice]['evts'] as $evt){
                $date = donneDateSemaineNumero($datas['annee'],$datas['semaine'],(int)$evt['jour_semaine']);
                $date_debut = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$evt['heure_debut'].':00');
                $date_fin = date_create_from_format("Y-m-d H:i:s",$date->format('Y-m-d').' '.$evt['heure_fin'].':00');

                $options = [
                    'organisateur'=>$evt['organisateur']??'',
                    'organisateur_email'=>$evt['organisateur_email']??'',
                    'confidentialite'=> $datas['confidentialite']??'2',
                    'status'=>$datas['status']??'confirmed',
                    'categorie'=>$evt['categorie']??[],
                    'couleur'=>$evt['couleur']??''
                ];


                if (!empty($options['categorie'])){
                    foreach ($options['categorie'] as &$cat){
                        if (!in_array($cat,$tab_cat)){
                            $cat='???';
                        }
                    }
                }

                $description = $caldavService->construireDescription($evt['observation'],$evt['nb_personne']??null,$evt['presentation']??null);


                $caldavService->addEvt(
                    $datas['serveur'],
                    $datas['calendrier'],
                    $evt['titre']??'',
                    $description??'',
                    $evt['lieu']??'',
                    $date_debut,
                    $date_fin,
                    $options,
                    true);
            }
            $this->sync($caldavService);
            $args_rep['ok'] = true;
            $this->addFlash('success','Evenement ajouté');
            $args_rep['url_redirect']=$this->generateUrl('agenda');
        }
        return $this->reponse_formulaire($form, $args_rep,'evenement/add_evts.html.twig');
    }



    #[Route(path: '/lot', name: 'evt_lot')]
    public function evt_lot(): Response
    {
        $tab_lotevt = $this->sac->conf('permanences',[]);
        $args_twig = ['tab_lot_evt'=>$tab_lotevt];
        return $this->render('evenement/lot.html.twig', $args_twig);
    }




    #[Route(path: '/lot/add', name: 'evt_lot_add')]
    public function evt_lot_add(): Response
    {
        $args_rep=[];

        $form = $this->generateForm('EvenementLot',EvenementLotType::class);

        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->em;
            $datas = $form->getData();
            $config = $em->getRepository(Config::class)->findOneBy(['nom'=>'permanences']);
            if (!$config){
                $config = new Config();
                $config->setNom('permanences');
            }
            $tab_valeur = $config->getValeur();
            if($tab_valeur===''){
                $tab_valeur=[];
            }
            $tab_valeur[]=[
                'nom' => $datas['nom'],
                'type' => $datas['type'],
                'evts' => []
            ];
            $config->setValeur($tab_valeur);
            $this->em->persist($config);
            $this->em->flush();
            $this->sac->initSac(true);
            $args_rep['url_redirect']=$this->generateUrl('evt_lot');
        }
        return $this->reponse_formulaire($form, $args_rep);

    }


    #[Route(path: '/lot/{indice}/add_evt', name: 'evt_lot_add_evt')]
    public function evt_lot_add_evt(int $indice): Response
    {
        $args_rep=[];
        $config = $this->em->getRepository(Config::class)->findOneBy(['nom'=>'permanences']);
        $tab_valeur = $config->getValeur();
        $datas = $tab_valeur[$indice]['data']??[];
        $args = ['indice'=>$indice];
        $form = $this->generateForm('evenementLotEvt',EvenementLotEvtType::class,$datas,[],false,$args);
        $args_rep['params'] = ['indice'=>$indice];
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();

            $tab_categorie=table_simplifier($this->sac->tab('categorie'));
            $tab_cat_select2 = json_decode((string) $form->get('categorie_add')->getData(),true);
            $tab_cat_new =[];
            if (!empty($tab_cat_select2)) {
                foreach ($tab_cat_select2 as $new_cat) {
                    if (!$new_cat['selected'] &&
                        $new_cat['id'] === $new_cat['text']) {
                        if (!in_array($new_cat['text'], $tab_categorie)) {
                            $categorie = new Categorie();

                            $categorie->setNom($new_cat['text']);
                            $this->em->persist($categorie);
                            $this->em->flush();
                            $tab_cat_new[] = $categorie->getNom();
                            $this->sac->initSac(true);
                        }
                    }
                }
            }
            $tab_lieu = table_simplifier($this->sac->tab('lieu'));
            $tab_lieu_new = json_decode((string) $form->get('lieu_add')->getData(),true);
            if (!empty($tab_lieu_new)){
                foreach($tab_lieu_new as $new_lieu){
                    if (!$new_lieu['selected'] &&
                        $new_lieu['id'] ===$new_lieu['text'] ){
                        if (!in_array($new_lieu['text'],$tab_lieu)){
                            $lieu = new Lieu();
                            $lieu->setNom($new_lieu['text']);
                            $this->em->persist($lieu);
                            $this->em->flush();
                            $datas['lieu']= $lieu->getNom();
                            $this->sac->initSac(true);
                        }
                    }
                }
            }

            $tab_valeur = $config->getValeur();
            $tab_valeur[$indice]['evts'][]=$datas;
            $config->setValeur($tab_valeur);
            $this->em->persist($config);
            $this->em->flush();
            $this->sac->initSac(true);
            $args_rep['url_redirect']=$this->generateUrl('evt_lot');
        }
        return $this->reponse_formulaire($form, $args_rep);

    }


    #[Route(path: '/lot/{indice}/{indice_evt}/modify_evt', name: 'evt_lot_modify_evt')]
    public function evt_lot_modify_evt(int $indice,int $indice_evt): Response
    {
        $args_rep=[];
        $config = $this->em->getRepository(Config::class)->findOneBy(['nom'=>'permanences']);
        $tab_valeur = $config->getValeur();
        $datas = $tab_valeur[$indice]['evts'][$indice_evt]??[];
        $args = ['indice'=>$indice,'indice_evt'=>$indice_evt];
        $form = $this->generateForm('permanenceEvt',EvenementLotEvtType::class,$datas,[],false,$args);
        $args_rep['params'] = ['indice'=>$indice,'indice_evt'=>$indice_evt];
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();
            $tab_valeur = $config->getValeur();
            $tab_valeur[$indice]['evts'][$indice_evt]=$datas;
            $config->setValeur($tab_valeur);
            $this->em->persist($config);
            $this->em->flush();
            $this->sac->initSac(true);
            $args_rep['url_redirect']=$this->generateUrl('evt_lot');
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    #[Route(path: '/lot/{indice}/{indice_evt}/supp_evt', name: 'evt_lot_supp_evt')]
    public function evt_lot_supp_evt(int $indice,int $indice_evt): Response
    {
        $args_rep=[];
        $config = $this->em->getRepository(Config::class)->findOneBy(['nom'=>'permanences']);
        $tab_valeur = $config->getValeur();
        if (isset($tab_valeur[$indice]['evts'][$indice_evt])){
            unset($tab_valeur[$indice]['evts'][$indice_evt]);
            $config->setValeur($tab_valeur);
            $this->em->persist($config);
            $this->em->flush();
            $this->sac->initSac(true);
        }
        return $this->redirectToRoute('evt_lot');
    }

    private function sync(CaldavService $caldavService){
        $date_debut = (new \DateTime())->sub(new \DateInterval('P4D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P6M'));
        $tab_serveur = $caldavService->getListeServeur();
        foreach($tab_serveur as $serveur){
            $caldavService->envoyerModificationAuServeurDistant($serveur);
            $caldavService->rechargeEvtDepuisServeurDistant($date_debut,$date_fin,$serveur);
        }
        return true;
    }


}

