<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\RgpdQuestion;


#[Route(path: '/admin/rgpd')]
class RgpdController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'rgpd_question_index', methods: 'GET')]
    public function index(): RedirectResponse|JsonResponse|Response|null
    {
        return $this->index_defaut("rgpd_question");
    }

    
    #[Route(path: '/new', name: 'rgpd_question_new', methods: 'GET|POST')]
    public function new(): RedirectResponse|JsonResponse|array|string|Response
    {
        return $this->new_defaut([],"rgpd_question");
    }
    
    #[Route(path: '/{id}/edit', name: 'rgpd_question_edit', methods: 'GET|POST')]
    public function edit(RgpdQuestion $ob): RedirectResponse|JsonResponse|array|string|Response
    {
        return $this->edit_defaut($ob,[],"rgpd_question");
    }
    
    
    #[Route(path: '/{id}', name: 'rgpd_question_delete', methods: 'DELETE')]
    public function delete(RgpdQuestion $ob): RedirectResponse|JsonResponse
    {
        return $this->delete_defaut($ob,"rgpd_question");
        
    }
    
    #[Route(path: '/{id}', name: 'rgpd_question_show', methods: 'GET')]
    public function show(RgpdQuestion $ob): RedirectResponse|Response
    {
        return $this->show_defaut($ob,"rgpd_question");
    }
}
