<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Participant;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendrierController extends Controller
{


    #[Route(path: '/calendrier/evts', name: 'calendrier_evts')]
    public function calendrier_evts(CaldavService $caldavService): Response
    {

        $date_debut = date_create_from_format(DATE_ATOM,$this->requete->get('start'));
        $date_fin = date_create_from_format(DATE_ATOM,$this->requete->get('end'));
        $tab_filtres=[];
        $calendriers = $this->requete->get('calendriers');
        if ($calendriers){
            $tab_filtres['calendriers']=$calendriers;
        }
        $public = $this->requete->get('public');
        if ($public){
            $tab_filtres['public']=1;
        }

        $evts = $caldavService->getEvtsGroupe($date_debut,$date_fin,false,$tab_filtres);

        $groupe_tmp = '';
        $tab_evts=[];
        foreach($evts as $ge){
            foreach($ge['evts'] as $e) {
                if ($groupe_tmp!=$ge) {
                    $titre = $e->getTitre();
                    if (count($ge['evts'])>1){
                        $titre = $ge['titre'];
                    }
                    $tab_evts[] = [
                        'id' => $e->getId(),
                        'title' => $titre,
                        'start' => $e->getDateDebut(),
                        'end' => $e->getDateFin(),
                        'color' => $e->getCouleur(),
                        'textColor' => couleurOppose($e->getCouleur()),
                        'extendedProps' => $e->toArray()
                    ];
                    $groupe_tmp=$ge;
                }
            }
        }
        return $this->json($tab_evts);
    }


    #[Route(path: '/calendrier/export', name: 'calendrier_export')]
    public function calendrier_export(CaldavService $caldavService): Response
    {
        $date_debut = (new \DateTime())->sub(new \DateInterval('P0D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P15D'));
        $tab_filtre_calendriers =$this->requete->get('filtre_calendriers');
        $evts = $caldavService->getEvtsGroupe($date_debut,$date_fin,false,$tab_filtre_calendriers);
        return $this->render('agenda/export.html.twig', ['evts'=>$evts]);
    }

}

