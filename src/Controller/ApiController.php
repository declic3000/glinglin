<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    #[Route(path: '/api/evts', name: 'api_evts')]
    public function evts(CaldavService $caldavService): Response
    {
        $date_debut = (new \DateTime())->sub(new \DateInterval('P0D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P7D'));
        $evts = $caldavService->getEvtsGroupe($date_debut, $date_fin);
        return $this->json($evts);
    }


    #[Route(path: '/api/taches', name: 'api_taches')]
    public function taches(CaldavService $caldavService): Response
    {
        $taches = $caldavService->getTODOS();
        return $this->json($taches);
    }


}
