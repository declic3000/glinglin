<?php

namespace App\Controller\Doc;

use App\Entity\Document;
use App\Entity\DocumentLien;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Selecteur;
use PHPImageWorkshop\ImageWorkshop;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


class DocumentController extends ControllerObjet
{



    #[Route(path: 'upload/{nom}', name: 'upload')]
    function upload(string $nom)
    {

        $session = $this->getSession();
        $output_dir = $this->sac->get('dir.root').'var/upload/';
        if (isset($_FILES['fichiers'])) {
            $ret = [];


            if (!file_exists($output_dir)) {
                if (!mkdir($output_dir) && !is_dir($output_dir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
                }
            }

            $ret['error'] = $_FILES['fichiers']["error"];
            if (!is_array($_FILES['fichiers']["name"])) //single file
            {

                $fileName = time() . '_' . $_FILES['fichiers']["name"];
                move_uploaded_file($_FILES['fichiers']["tmp_name"], $output_dir . $fileName);
                $ret['files'][] = $fileName;
            } else  //Multiple files, file[]
            {
                $fileCount = count($_FILES['fichiers']["name"]);
                for ($i = 0; $i < $fileCount; $i++) {
                    $fileName = time() . '_' . $_FILES['fichiers']["name"][$i];
                    move_uploaded_file($_FILES['fichiers']["tmp_name"][$i], $output_dir . $fileName);

                    $ret['files'][] = $fileName;
                }

            }
            $session->set('file_upload_tmp_' . $nom, $ret['files']);
        }

        return $this->json($ret);
    }




}

