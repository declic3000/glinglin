<?php
namespace App\Controller;


use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Lieu;


#[Route(path: '/admin/lieu')]
class LieuController extends ControllerObjet
{
    
    #[Route(path: '/', name: 'lieu_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    
    #[Route(path: '/new', name: 'lieu_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }
    
    #[Route(path: '/{id}/edit', name: 'lieu_edit', methods: 'GET|POST')]
    public function edit(lieu $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    #[Route(path: '/{id}', name: 'lieu_delete', methods: 'DELETE')]
    public function delete(lieu $ob)
    {
        return $this->delete_defaut($ob);
    }


    #[Route(path: '/{id}/logo', name: 'lieu_logo', methods: 'GET|POST')]
    public function logo(lieu $ob,Ged $ged)
    {
        return $this->logo_defaut($ged,$ob);
    }


    #[Route(path: '/{id}', name: 'lieu_show', methods: 'GET|POST')]
    public function show(lieu $ob)
    {
        $args_twig = [
            'objet_data' => $ob
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
}
