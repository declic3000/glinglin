<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Contact;
use App\Entity\Participant;
use App\Service\CaldavService;
use App\Service\CookieService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AgendaController extends Controller
{
    #[Route(path: '/', name: 'agenda')]
    public function agenda(CaldavService $caldavService,CookieService $cookieService): Response
    {
        $date_debut = (new \DateTime());
        $date_fin = (new \DateTime())->add(new \DateInterval('P6M'));
        $date_debut_mois =  \DateTime::createFromFormat('Ymd',(new \DateTime())->format('Ym'.'01'));
        $date_mois_suivant = (clone($date_debut_mois))->add(new \DateInterval('P1M'));
        $consultation = $this->requete->get('consultation');
        $tab_filtres=[];
        $calendriers = $this->requete->get('calendriers');
        if ($calendriers){
            $tab_filtres['calendriers']=$calendriers;
        }
        $public = $this->requete->get('public');
        if ($public){
            $tab_filtres['public']=1;
        }

        $evts = $caldavService->getEvtsGroupe($date_debut,$date_fin,false,$tab_filtres);
        $tab_cle=[];
        foreach($evts as $evt){
            foreach($evt['evts'] as $e){
                foreach($e->getParticipants() as $participant) {
                    $tab_cle[] = $participant->getCle();
                }
                foreach($e->getOrganisateurs() as $organisateur) {
                    $tab_cle[] = $organisateur->getCle();
                }
            }
        }
        $tab_cle = array_unique($tab_cle);

        $tab_calendrier = $caldavService->getListeServeurCalendrier();
        $cats = [];
        $lieux = [];
        foreach($evts as $evt){
            $cats = array_merge($cats,$evt['categorie']);
            $lieux = array_merge($lieux,$evt['lieux']);
        }
        $cats = array_unique($cats);
        $lieux = array_unique($lieux);
        $tab_evts_date = array_keys($evts);
        foreach($tab_evts_date as &$evt){
            $evt = substr($evt,0,8);
        }
        $nb_jour_mois_en_cours = cal_days_in_month(CAL_GREGORIAN, $date_debut_mois->format('m'), $date_debut_mois->format('Y'));
        $nb_jour_mois_suivant = cal_days_in_month(CAL_GREGORIAN, $date_mois_suivant->format('m'), $date_mois_suivant->format('Y'));
        $tab_mois = [
            'courant'=>[
                'date' => $date_debut_mois,
                'jour1' => (((int)$date_debut_mois->format('w'))+6)%7,
                'nb_jour' => $nb_jour_mois_en_cours,
                'jours_evt'=>[]
            ],
            'suivant'=>[
                'date' => $date_mois_suivant,
                'jour1' => (((int)$date_mois_suivant->format('w'))+6)%7,
                'nb_jour' => $nb_jour_mois_suivant,
                'jours_evt'=>[]
            ],
        ];
        $jour_de_la_semaine = [
            'L' => 'Lundi',
            'Ma' => 'Mardi',
            'Me' => 'Mercredi',
            'J' => 'Jeudi',
            'V' => 'Vendredi',
            'S' => 'Samedi',
            'D' => 'Dimanche'
        ];
        for ($i=1;$i<=$nb_jour_mois_en_cours;$i++){
            $tab_mois['courant']['jours_evt'][$i]=in_array($date_debut->format('Ym').str_pad($i,2,'0',STR_PAD_LEFT),$tab_evts_date);
        }
        for ($i=1;$i<=$nb_jour_mois_suivant;$i++){
            $tab_mois['suivant']['jours_evt'][$i]=in_array($date_mois_suivant->format('Ym').str_pad($i,2,'0',STR_PAD_LEFT),$tab_evts_date);;
        }
        
        $res = new Response();
        $pref = $cookieService->getJson('glinglin_pref');

        $tab_mode_affichage = getModeAffichage();
        $config_affichage = $this->conf('affichage');
        $mode_affichage = $pref['mode_affichage'] ?? $config_affichage['mode_affichage'];
        $r_mode_affichage =$this->requete->get('mode_affichage');
        if ( $r_mode_affichage !==null   ){
            $pref =  [
                'mode_affichage'=>$r_mode_affichage
                ];
            $cookie = $cookieService->cookie('glinglin_pref',$pref,365*2*24*3600);
            $res->headers->setCookie($cookie);
            $mode_affichage = $r_mode_affichage;
        }
        $caracteres=[];
        $rgpd=[];
        $contact = $this->em->getRepository(Contact::class)->findOneBy(['cle'=>$cookieService->getCleIdentite()]);
        if ($contact)
        {
            $caracteres=$contact->getCaracteristiquesId();
            $rgpd=$contact->getRgpdReponsesValeurs();
        }
        $caracteres_contact=[];
        $tab_contact = $this->em->getRepository(Contact::class)->findBy(['cle'=>$tab_cle]);
        foreach($tab_contact as $contact){
            $caracteres_contact[$contact->getCle()] = $contact->getCaracteristiquesId();
        }


        if ($public){
        $conf_page_publique = $this->conf('page_publique');
            $titre= $conf_page_publique['titre'];
            $texte= $conf_page_publique['texte'];
        }else{
            $conf_generale = $this->conf('general');
            $titre= $conf_generale['slogan'];
            $texte= $conf_generale['explicatif'];
        }

        $args_twig=[
            'evts'=>$evts,
            'time'=>time(),
            'cats'=>$cats,
            'lieux'=>$lieux,
            'date_debut'=>$date_debut,
            'config_affichage'=>$config_affichage,
            'tab_mois'=>$tab_mois,
            'tab_mode_affichage'=>$tab_mode_affichage,
            'mode_affichage'=>$mode_affichage,
            'jour_de_la_semaine'=>$jour_de_la_semaine,
            'tab_calendrier'=>$tab_calendrier,
            'js_suppl'=>'scripts_calendar',
            'tab_filtres' => $tab_filtres,
            'consultation' => $consultation,
            'identite' => $cookieService->getIdentite(),
            'rgpd' => $rgpd,
            'caracteres' => $caracteres,
            'cc' => $caracteres_contact,
            'titre' => $titre,
            'texte' => $texte
            ];
        return $this->render('agenda/index.html.twig',$args_twig,$res);
    }


    #[Route(path: '/public', name: 'agenda_public')]
    public function agenda_public(CaldavService $caldavService,CookieService $cookieService): Response
    {
        $conf_page_publique = $this->conf('page_publique');
        if ($conf_page_publique['active']){
              $request = $this->requete->getRequest()->request;
              $request->set('consultation','1');
              $request->set('public','1');
              return $this->agenda($caldavService,$cookieService);
        }else{
            return $this->redirectToRoute('erreur404');
        }
    }


    #[Route(path: '/export', name: 'agenda_export')]
    public function agenda_export(CaldavService $caldavService): Response
    {
        $date_debut = (new \DateTime())->sub(new \DateInterval('P0D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P15D'));
        $evts = $caldavService->getEvtsGroupe($date_debut,$date_fin);
        return $this->render('agenda/export.html.twig', ['evts'=>$evts]);
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    #[Route(path: '/agenda/nouveaute', name: 'agenda_nouveaute')]
    public function agenda_nouveaute(CaldavService $caldavService): Response
    {
        $db = $this->em->getConnection();
        $date  = $db->fetchOne('select updated_at from calevent order by updated_at desc limit 1');
        $date = new \DateTime($date);
        return $this->json(['date'=>$date->getTimestamp()]);
    }


    #[Route(path: '/agenda/evt', name: 'agenda_evt')]
    public function agenda_evt(CookieService $cookieService,CaldavService $caldavService): Response
    {
        $id = $this->requete->get('id');
        if ($this->requete->estAjax()){
            $gevt = $caldavService->getEvtGroupe($id);
            $html = $this->renderElt($cookieService,$gevt);
            return $this->json(['ok'=>true,'html'=>$html]);
        }
        else{
            return $this->redirectToRoute('agenda');
        }
    }



    #[Route(path: '/agenda/add_comment', name: 'agenda_add_comment')]
    public function agenda_add_comment(CookieService $cookieService,CaldavService $caldavService): Response
    {

        $id = $this->requete->get('id');
        $identite = $cookieService->getCleIdentite();
        if (!empty($identite)) {
            $cal = $this->em->getRepository(Cal::class)->find($id);
            $commentaire = $this->requete->get('commentaire');
            $cal->addCommentaire($identite, $commentaire);
            $caldavService->addCalCommentaire($cal);
            $cal->setAReporter(true);
            $this->em->persist($cal);
            $this->em->flush();
            if ($this->requete->estAjax()){
                $gevt = $caldavService->getEvtGroupe($id);
                $html = $this->renderElt($cookieService,$gevt);
                return $this->json(['ok'=>true,'message'=>'Commentaire ajouté','html'=>$html]);
            }
            else {
                $this->addFlash('success','Commentaire ajouté');
                return $this->redirectToRoute('agenda');
            }
        }
        return $this->redirectToRoute('error403');
    }




    #[Route(path: '/agenda/add_participation', name: 'agenda_add_participation')]
    public function agenda_add_participation(CaldavService $caldavService,CookieService $cookieService): Response
    {
        $id = $this->requete->get('id');
        $tab_identite = $cookieService->getIdentite();
        if (!empty($tab_identite)) {
            $cle_identite = $tab_identite['cle'];
            $nom = $tab_identite['nom'];
            $email = $tab_identite['email'];
            $cal = $this->em->getRepository(Cal::class)->find($id);
            $cal->setCal($caldavService->addParticipantEvent($cal->getCal(),$nom,$email));
            $tab_personnes = $cal->getPersonnesJson();
            $tab_personnes[$cle_identite] = $tab_identite;
            $cal->setPersonnesJson($tab_personnes);
            $cal->setAReporter(true);
            $this->em->persist($cal);
            $this->em->flush();

            if ($this->requete->estAjax()){
                $gevt = $caldavService->getEvtGroupe($id);
                $html = $this->renderElt($cookieService,$gevt);
                return $this->json(['ok'=>true,'message'=>'Participant ajouté','html'=>$html]);
            }
            else{

                $this->addFlash('success','Participant ajouté');
                return $this->redirectToRoute('agenda');
            }


        }
        return $this->redirectToRoute('error403');

    }

    #[Route(path: '/agenda/remove_participation', name: 'agenda_remove_participation')]
    public function agenda_remove_participation(CookieService $cookieService,CaldavService $caldavService): Response
    {

        $id = $this->requete->get('id');
        $identite = $cookieService->getCleIdentite();
        if (!empty($identite)) {
            $identite = $cookieService->getIdentite();
            $cle_identite = $identite['cle'];
            $cal = $this->em->getRepository(Cal::class)->find($id);
            $cal->setCal($caldavService->removeParticipantEvent($cal->getCal(),$identite['nom'],$identite['email']));
            $tab_personnes = $cal->getPersonnesJson();
            unset($tab_personnes[$cle_identite]);
            $cal->setPersonnesJson($tab_personnes);
            $cal->setAReporter(true);
            $this->em->persist($cal);
            $this->em->flush();
            if ($this->requete->estAjax()){
               $gevt = $caldavService->getEvtGroupe($id);
                $html = $this->renderElt($cookieService,$gevt);
                return $this->json(['ok'=>true,'message'=>'Participation retirée','html'=>$html]);
            }
            else{
                $this->addFlash('success','Participation retiré');
                return $this->redirectToRoute('agenda');
            }
        }

        return $this->redirectToRoute('error403');
    }


    function renderElt($cookieService,$gevt): string
    {
        $pref = $cookieService->getJson('glinglin_pref');
        $config_affichage = $this->conf('affichage');
        $mode_affichage = $pref['mode_affichage'] ?? $config_affichage['mode_affichage'];
        $template = $mode_affichage==='liste_compacte' ? 'evt_tr':'evt_card';
        $consultation = $this->requete->get('consultation',0);
        $caracteres_contact=[];
        $tab_cle=[];


            foreach($gevt['evts'] as $e){
                foreach($e->getParticipants() as $participant) {
                    $tab_cle[] = $participant->getCle();
                }
                foreach($e->getOrganisateurs() as $organisateur) {
                    $tab_cle[] = $organisateur->getCle();
                }
            }

        $tab_cle = array_unique($tab_cle);
        $tab_contact = $this->em->getRepository(Contact::class)->findBy(['cle'=>$tab_cle]);
        foreach($tab_contact as $contact){
            $caracteres_contact[$contact->getCle()] = $contact->getCaracteristiquesId();
        }
        $args_twig=[
            'gevt'=>$gevt,
            'consultation'=>$consultation,
            'cc'=>$caracteres_contact
        ];

        return $this->renderView( 'agenda/'.$template.'.html.twig',$args_twig);

    }

}

