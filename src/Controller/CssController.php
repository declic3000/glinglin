<?php

namespace App\Controller;

use App\Service\CaldavService;
use Declic3000\Pelican\Service\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CssController extends Controller
{
    #[Route(path: '/css/personalisation.css', name: 'css_personalisation')]
    public function css_personalisation(): Response
    {
        $config = $this->conf('affichage');
        $args_twig = [
            'couleur_du_fond'=>$config['couleur_du_fond']
        ];
        $config = $this->conf('logo');
        if (!empty($config['logo_perso'])){
            if (file_exists($this->sac->get('dir.root').'public/assets/perso/'.$config['logo_perso'])){
                $args_twig['logo_perso'] = '/assets/perso/'.$config['logo_perso'];
            }
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'text/css');
        return $this->render('css/personalisation.css.twig', $args_twig,$response);
    }

    #[Route(path: '/cuisson', name: 'cuisson')]
    public function cuisson(): Response
    {
        $nom = $this->requete->get('nom');
        $email = $this->requete->get('email');
        if (!empty($nom) && $nom !='@nom@'){
            $cookie = new Cookie(
                'glinglin',	// Cookie name.
                $nom.':'.$email,	// Cookie value.
                time() + ( 2 * 365 * 24 * 60 * 60)	// Expires 2 years.
            );
        }
        $ancre = $this->requete->get('ancre','');
        $ancre = ($ancre==='')?'':'#'.$ancre;
        $url = $this->requete->getRequest()->query->get('redirect',$this->generateUrl('agenda')).$ancre;
        $res = new RedirectResponse($url);
        if (isset($cookie)) {
            $res->headers->setCookie($cookie);
        }
        return $res;
    }



    #[Route(path: '/adios', name: 'adios')]
    public function adios(): Response
    {
        $url = $this->requete->get('redirect',$this->generateUrl('agenda'));
        $res = new RedirectResponse($url);
        $res->headers->clearCookie('glinglin');
        return $res;
    }




}
