<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Categorie;
use App\Entity\Config;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Form\AddEvtsType;
use App\Form\EvtType;
use App\Form\EvenementLotEvtType;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route(path: '/admin')]
class AdminController extends Controller
{
    #[Route(path: '/', name: 'admin')]
    public function agenda(CaldavService $caldavService): Response
    {
        $this->addFlash('success','Bienvenue dans l\'espace d\'administration, vous êtes connecté en tant qu\'administrateur');
        $args_twig=[];
        return $this->render('admin/index.html.twig',$args_twig);
    }


    #[Route(path: '/agenda_sync', name: 'agenda_sync')]
    public function agenda_sync(CaldavService $caldavService): Response
    {
        $this->sync($caldavService);
        return $this->redirectToRoute('agenda');
    }

    private function sync(CaldavService $caldavService){
        $date_debut = (new \DateTime())->sub(new \DateInterval('P4D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P6M'));
        $tab_serveur = $caldavService->getListeServeur();
        foreach($tab_serveur as $serveur){
            $caldavService->envoyerModificationAuServeurDistant($serveur);
            $caldavService->rechargeEvtDepuisServeurDistant($date_debut,$date_fin,$serveur);
        }
        return true;
    }





}

