<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Participant;
use App\Service\CaldavService;
use App\Service\RaspismsService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route(path: '/admin/relance')]
class RelanceController extends Controller
{
    #[Route(path: '/', name: 'relance')]
    public function sms(RaspismsService $raspismsService): Response
    {
        $args_twig=[];
        return $this->render('relance/index.html.twig',$args_twig);
    }


}

