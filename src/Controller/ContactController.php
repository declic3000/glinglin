<?php
namespace App\Controller;


use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\contact;


#[Route(path: '/admin/contact')]
class ContactController extends ControllerObjet
{
    
    #[Route(path: '/', name: 'contact_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    
    #[Route(path: '/new', name: 'contact_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }
    
    #[Route(path: '/{id}/edit', name: 'contact_edit', methods: 'GET|POST')]
    public function edit(contact $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    #[Route(path: '/{id}', name: 'contact_delete', methods: 'DELETE')]
    public function delete(contact $ob)
    {
        return $this->delete_defaut($ob);
    }


    #[Route(path: '/{id}/logo', name: 'contact_logo', methods: 'GET|POST')]
    public function logo(contact $ob,Ged $ged)
    {
        return $this->logo_defaut($ged,$ob);
    }


    #[Route(path: '/{id}', name: 'contact_show', methods: 'GET|POST')]
    public function show(contact $ob)
    {
        $args_twig = [
            'objet_data' => $ob
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
}
