<?php

namespace App\Controller\Sys;


use App\Entity\Article;
use App\Query\ArticleQuery;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\LogMachine;
use App\Service\Photographe;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Suc;
use PHPImageWorkshop\ImageWorkshop;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Annotation\Route;


#[IsGranted("ROLE_ADMIN")]
#[Route(path: '/test')]
class TestController extends Controller
{


    #[Route(path: '/', name: 'test')]
    public function test()
    {
        return $this->render('sys/test/index.html.twig',['onglet'=>'test']);
    }


    #[Route(path: '/mail', name: 'test_mail')]
    public function test_mail(Facteur $facteur)
    {

        $email = $this->requete->get('email');


        if($email){
            $ok = $facteur->courriel_twig($email, 'test', ['date'=>new \DateTime()]);
            if($ok){
                $this->addFlash('info', 'L\'email de test vient de vous être envoyer');
            }else{
                $this->addFlash('error', 'Problème dans l\'envoi de l\'email, vérifier la configuration');
            }
        }
        $email = empty($email)?$this->suc->get('operateur.email'):$email;
        $args_twig=[
            'onglet'=>'test',
            'email'=>$email
        ];
        return $this->render('sys/test/mail.html.twig',$args_twig);
    }

    #[Route(path: '/test', name: 'test_test')]
    public function test_test()
    {
        $description = "Chaque mercredi soir, l'association propose une rencontre pour partager des connaissances, des savoir-faire, des questions autour de l'utilisation des logiciels libres, que ce soit à propos du système d'exploitation Linux, des applications libres ou des services en ligne libres.\n\nC'est l'occasion aussi de mettre en avant l'action des associations fédératrices telles que l'April ou Framasoft, dont nous sommes adhérents et dont nous soutenons les initiatives avec grande reconnaissance.\n\n\n----------- COMMENTAIRES -----------\nguillaume: :\ntest xomm\nfsdioqfodsqhfiodsq\n";
        dump(CaldavService::extraireDescriptif($description));
        dump(CaldavService::extraireNbPersonne($description));
        dump(CaldavService::extrairePresentation($description));
        exit();
    }

}
