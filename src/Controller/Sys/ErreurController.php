<?php

namespace App\Controller\Sys;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class ErreurController extends AbstractController
{
    #[Route(path: '/erreur403', name: 'erreur403')]
    public function erreur403()
    {
        $message = 'Erreur : Vous n\'êtes pas autorisé à consulter cette page';
        return $this->render('page_erreur.html.twig', ['message' => $message]);

    }


    #[Route(path: '/erreur404', name: 'erreur404')]
    public function erreur404()
    {
        $message = 'Erreur : Cette page n\'existe pas ou n\'est pas accessible';
        return $this->render('page_erreur.html.twig', ['message' => $message]);

    }


    #[Route(path: '/erreur500', name: 'erreur500')]
    public function erreur500()
    {
        $message = 'Oups! Une erreur est survenue. L\'administrateur a été prévenu. ' ;
        return $this->render('page_erreur.html.twig', ['message' => $message]);

    }



}
