<?php

namespace App\Controller\Sys;

use App\Component\Form\PreferenceFormBuilder;
use App\Entity\Categorie;
use App\Entity\Config;
use App\Entity\Lieu;
use App\Form\EvenementLotEvtType;
use App\Form\EvenementLotType;
use App\Service\CaldavService;
use App\Service\Initialisator;
use Declic3000\Pelican\Entity\Entity;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
#[Route(path: '/admin/config')]
class ConfigController extends Controller
{
    #[Route(path: '/', name: 'config')]
    public function index(PreferenceFormBuilder $pref_form,Initialisator $initialisator)
    {
        $tab_entree = $initialisator->getConfigDefaut();
        $choix = [];

        foreach ($tab_entree as $k => $entree) {
            if (!isset($entree['systeme'])) {
                $data = $this->sac->conf($k);
                if (empty($data)){
                    $data=null;
                }
                if (isset($entree['variables'][ 'logo_perso'])){
                    $data['logo_perso']=null;
                }
                $options = ['attr' => ['data-url' => $this->generateUrl('upload', ['nom' => 'config'])]];
                $form =  $pref_form->transforme_en_formulaire($k, $entree['variables'],$data,'config',100,[],null,$options);
                $choix[$k] = $form->createView();
            }
        }
        $args_twig = ['choix'=>$choix];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }













}
