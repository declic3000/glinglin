<?php

namespace App\Controller\Sys;


use App\Service\Initialisator;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route(path: '/admin/init')]
class InitController extends Controller
{
    #[Route(path: '/mot', name: 'init_mot')]
    public function init_mot(Initialisator $init)
    {

        $init->initialiser_mot();
        $this->sac->initSac(true);
        $this->addFlash('info', 'Les mots système ont bien été réinitialisés');
        return $this->redirectToRoute('tools',['onglet'=>'table']);
    }

    
    #[Route(path: '/config', name: 'init_config')]
    public function init_config(Initialisator $init)
    {
        
        $init->config_maj();
        $init->verifier_config();
        $this->sac->initSac(true);
        $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        return $this->redirectToRoute('tools',['onglet'=>'config']);
    }


    #[Route(path: '/init_config_periode', name: 'init_config_periode')]
    public function init_config_periode(Initialisator $init)
    {
        $dir=$this->sac->get('dir');
        $chargeur_sac = new \App\Init\ChargeurSac($this->em->getConnection(), new Bigben($this->translator));
        $tab_data = $this->sac->conf('systeme');

        // Calendrier comptable
        $tab_calendrier_comptable=[];
        $mois_jour = $this->sac->conf('pre_compta.date_debut_exercice')->format('m-d');
        $db= $this->em->getConnection();
        $premiere_annee = $db->fetchFirstColumn('select YEAR(date_enregistrement) from asso_paiements where date_enregistrement is not null order by date_enregistrement');
        if (!$premiere_annee)
            $premiere_annee=2000;
        for($i=$premiere_annee;$i<2100;$i++){
            $tab_calendrier_comptable[]= ($i).'-'.$mois_jour;
        }
        $tab_data['calendrier_comptable'] = $tab_calendrier_comptable;


        // Periode cotisation abonnement


        $tab_prestation=$this->sac->tab('prestation');
        $tab_prestation_type=$this->sac->tab('prestation_type');

        $tab_periode=$chargeur_sac->getDateCalendrierPrestationType($tab_prestation,$tab_prestation_type);


        foreach ($tab_periode as $ob=>&$tab_prestation_groupe){
            foreach ($tab_prestation_groupe as $prestation=>&$tab_p) {
                    foreach($tab_p as &$p){
                        $p=$p['date']->format('Y-m-d');
                    }
                    $el = end($tab_p);

                    if($el) {
                        $derniere_annee = substr((string) $el,0,4);
                        $mois_jour = substr((string) $el,5);
                    }
                    else
                    {
                        $derniere_annee = date('Y')-11;
                        $mois_jour = '01-01';
                    }
                    $indice = count($tab_p);
                    for($i=$derniere_annee+1;$i<2050;$i++) {
                        $tab_p['periode'.($indice)] = ($i) . '-' . $mois_jour;
                        $indice++;
                    }
            }
        }

        $tab_data['periode'] = $tab_periode;
        $tab_data = json_encode($tab_data);
        $db->update('sys_configs', ['variables'=>$tab_data],['nom'=>'systeme']);
        $init->verifier_config();
        $this->sac->initSac(true);
        $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        return $this->redirectToRoute('tools',['onglet'=>'config']);
    }



    
    
    #[Route(path: '/preference', name: 'init_preference')]
    public function init_preference(Initialisator $init)
    {

        $init->preference_maj();
        $init->verifier_preference();
        $this->sac->initSac(true);
        $this->addFlash('info', 'La table des preferences a bien été réinitialisé');
        return $this->redirectToRoute('tools',['onglet'=>'variables']);
    }
    
    
    #[Route(path: '/bloc', name: 'init_bloc')]
    public function init_bloc(Initialisator $init)
    {
        $init->initialiser_bloc();
        $this->sac->initSac(true);
        $init->initialiser_courrier();
        $this->sac->initSac(true);
        $this->addFlash('info', 'Les élément de modèles ont bien été réinitialisés');
        return $this->redirectToRoute('tools',['onglet'=>'config']);
    }


    #[Route(path: '/unite', name: 'init_unite')]
    public function init_unite(Initialisator $init)
    {

        $init->initialiser_unite();
        $this->sac->initSac(true);
        $this->addFlash('info', 'Les unités viennent  ont bien été réinitialisés');
        return $this->redirectToRoute('tools',['onglet'=>'table']);
    }



}
