<?php

namespace App\Controller;

use App\Entity\Caracteristique;
use App\Entity\Contact;
use App\Entity\RgpdQuestion;
use App\Entity\RgpdReponse;
use App\Service\CaldavService;
use App\Service\CookieService;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    #[Route(path: '/accueil', name: 'app_glinglin')]
    public function index(CaldavService $caldavService): Response
    {

        $date_debut = (new \DateTime())->sub(new \DateInterval('P7D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P6M'));
        $evts = $caldavService->getEvts($date_debut,$date_fin);
        $taches = $caldavService->getTaches();

        $args_twig = [
                'evts' => $evts,
                'taches' => $taches
            ];
        return $this->render('glinglin/index.html.twig', $args_twig);
    }

    #[Route(path: '/cuisson', name: 'cuisson')]
    public function cuisson(CookieService $cookieService): Response
    {
        $nom = $this->requete->get('nom');
        $email = $this->requete->get('email');
        $telephone = $this->requete->get('telephone');

        if (!empty($nom) && $nom !='@nom@'){
            $cle = $nom.':'.$email;
            $data = ['cle'=>$cookieService->protectionCle($nom.':'.$email),'nom'=>$nom,'email'=>$email];
            $cookie = $cookieService->cookie('glinglin',$data,3600*24*365*2);
            $clet=$cookieService->protectionCle($nom.':'.$email);
            $contact = $this->em->getRepository(Contact::class)->findOneBy(['cle'=>$clet]);
            if(!$contact){
                $contact = new Contact();
                $contact->setCle($clet);
                $contact->setNom($nom);
                $contact->setEmail($email);
                $contact->setTelephone($telephone);
                $this->em->persist($contact);
                $this->em->flush();
            }
        }
        if ($contact){
            $this->traitementFormulaireCaractere($contact->getCle());
            $this->traitementFormulaireRgpd($contact->getCle());
        }
        $ancre = $this->requete->get('ancre','');
        $ancre = ($ancre==='')?'':'#'.$ancre;
        $url = $this->requete->getRequest()->query->get('redirect',$this->generateUrl('agenda')).$ancre;
        $res = new RedirectResponse($url);
        if (isset($cookie)) {
            $res->headers->setCookie($cookie);
        }
        return $res;
    }



    #[Route(path: '/caractere', name: 'caractere')]
    public function caractere(CookieService $cookieService): Response
    {
        $cle = $cookieService->getCleIdentite();
        $this->traitementFormulaireCaractere($cle,true);
        return $this->json(['ok'=>true,'message'=>'Modification enregistrée','redirect'=>$this->generateUrl('agenda')]);
    }

    #[Route(path: '/rgpd', name: 'rgpd')]
    public function rgpd(CookieService $cookieService): Response
    {
        $cle = $cookieService->getCleIdentite();
        $this->traitementFormulaireRgpd($cle,true);

        return $this->json(['ok'=>true,'message'=>'Modification enregistrée','redirect'=>$this->generateUrl('agenda')]);
    }


    public function traitementFormulaireCaractere($cle,$reset=false)
    {
        $contact = $this->em->getRepository(Contact::class)->findOneBy(['cle'=>$cle]);
        $data = $this->requete->all('carac');

        if (!empty($data) || $reset){
            if(!is_array($data)){
                $data =[$data];
            }
            if($contact){
                $tab_carac = $this->tab('caracteristique');
                $tab_temp = [];
                foreach($tab_carac as $id=>$carac) {
                    if (in_array($id,$data)) {
                        $tab_temp[] = $id;
                    }
                }
                $tab_carac = $this->em->getRepository(Caracteristique::class)->findBy(['id'=>$tab_temp]);
                $contact->changeCaracteristique($tab_carac);
                $this->em->persist($contact);
                $this->em->flush();
            }
        }
    }

 public function traitementFormulaireRgpd($cle,$reset=false){
    $contact = $this->em->getRepository(Contact::class)->findOneBy(['cle'=>$cle]);
    $tab_question = $this->em->getRepository(RgpdQuestion::class)->findAll();
    $tab_reponses_o = $this->em->getRepository(RgpdReponse::class)->findBy(['contact'=>$contact]);
    $tab_reponses= [];
    foreach($tab_reponses_o as $rep){
        $tab_reponses[$rep->getRgpdQuestion()->getId()]=$rep;
    }
    $data = $this->requete->get('rgpd',[]);

     if (!empty($data) || $reset){
        if (!is_array($data)){
            $data=[$data];
        }
        foreach($tab_question as $question) {
            if (!isset($tab_reponses[$question->getId()])){
                $rep = new RgpdReponse();
                $rep->setRgpdQuestion($question);
                $rep->setContact($contact);
                $tab_reponses[$question->getId()] = $rep;
            }
            else{
                $rep = $tab_reponses[$question->getId()];
            }

            $rep->setValeur(in_array($question->getId(),$data)?1:0);
            $this->em->persist($rep);

        }

        $this->em->flush();
     }
}



    #[Route(path: '/adios', name: 'adios')]
    public function adios(): Response
    {
        $url = $this->requete->get('redirect',$this->generateUrl('agenda'));
        $res = new RedirectResponse($url);
        $res->headers->clearCookie('glinglin');
        return $res;
    }




}
