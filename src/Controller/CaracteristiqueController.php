<?php
namespace App\Controller;


use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Caracteristique;


#[Route(path: '/admin/caracteristique')]
class CaracteristiqueController extends ControllerObjet
{
    
    #[Route(path: '/', name: 'caracteristique_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    
    #[Route(path: '/new', name: 'caracteristique_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }
    
    #[Route(path: '/{id}/edit', name: 'caracteristique_edit', methods: 'GET|POST')]
    public function edit(Caracteristique $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    #[Route(path: '/{id}', name: 'caracteristique_delete', methods: 'DELETE')]
    public function delete(Caracteristique $ob)
    {
        return $this->delete_defaut($ob);
    }


    #[Route(path: '/{id}/logo', name: 'caracteristique_logo', methods: 'GET|POST')]
    public function logo(Caracteristique $ob,Ged $ged)
    {
        return $this->logo_defaut($ged,$ob);
    }


    #[Route(path: '/{id}', name: 'caracteristique_show', methods: 'GET|POST')]
    public function show(Caracteristique $ob)
    {
        $args_twig = [
            'objet_data' => $ob
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
}
