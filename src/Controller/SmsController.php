<?php

namespace App\Controller;

use App\Entity\Cal;
use App\Entity\Participant;
use App\Service\CaldavService;
use App\Service\RaspismsService;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route(path: '/sms')]
class SmsController extends Controller
{
    #[Route(path: '/', name: 'sms')]
    public function sms(RaspismsService $raspismsService): Response
    {
        $args_twig=[];
        return $this->render('admin/index.html.twig',$args_twig);
    }
    #[Route(path: '/test/', name: 'sms_test')]
    public function sms_test(RaspismsService $raspismsService): Response
    {
        $raspismsService->envoyerSms(['+33695334259'],"coucou micro");
        $this->addFlash('success','Le sms a été envoyé');
        return $this->redirectToRoute('sms');
    }

}

