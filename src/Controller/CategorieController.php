<?php
namespace App\Controller;


use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Categorie;


#[Route(path: '/admin/categorie')]
class CategorieController extends ControllerObjet
{
    
    #[Route(path: '/', name: 'categorie_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    
    #[Route(path: '/new', name: 'categorie_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }
    
    #[Route(path: '/{id}/edit', name: 'categorie_edit', methods: 'GET|POST')]
    public function edit(Categorie $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    #[Route(path: '/{id}', name: 'categorie_delete', methods: 'DELETE')]
    public function delete(Categorie $ob)
    {
        return $this->delete_defaut($ob);
    }


    #[Route(path: '/{id}/logo', name: 'categorie_logo', methods: 'GET|POST')]
    public function logo(Categorie $ob,Ged $ged)
    {
        return $this->logo_defaut($ged,$ob);
    }


    #[Route(path: '/{id}', name: 'categorie_show', methods: 'GET|POST')]
    public function show(Categorie $ob)
    {
        $args_twig = [
            'objet_data' => $ob
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
}
