<?php

namespace App\Controller;

use App\Service\AgendaService;
use App\Service\CaldavService;
use App\Service\GestsupService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TacheController extends AbstractController
{
    #[Route(path: '/taches', name: 'taches')]
    public function taches(CaldavService $caldavService): Response
    {
        //$caldavService->init('agenda_declic');
        $taches = $caldavService->getTaches(null,true);

        return $this->render('tache/index.html.twig', ['evts'=>$taches]);
    }
}
