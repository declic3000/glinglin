<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Categorie
 */
#[ORM\Table(name: 'caracteristique')]
#[ORM\Index(name: 'caracteristique_nom', columns: ['nom'])]
#[ORM\Index(name: 'caracteristique_famille', columns: ['famille'])]
#[ORM\Entity]
class Caracteristique extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 80, nullable: false)]
    protected $nom;


    /**
     * @var string
     */
    #[ORM\Column(name: 'symbole', type: 'string', length: 2, nullable: false)]
    protected $symbole;


    /**
     * @var ?string
     */
    #[ORM\Column(name: 'descriptif', type: 'text', nullable: true)]
    protected $descriptif;


    /**
     * @var ?string
     */
    #[ORM\Column(name: 'famille', type: 'string', length: 20, nullable: true)]
    protected $famille;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getSymbole(): string
    {
        return $this->symbole;
    }

    public function setSymbole(string $symbole): void
    {
        $this->symbole = $symbole;
    }

    public function getDescriptif(): string
    {
        return $this->descriptif;
    }

    public function setDescriptif(?string $descriptif): void
    {
        $this->descriptif = $descriptif;
    }

    public function getFamille(): ?string
    {
        return $this->famille;
    }

    public function setFamille(?string $famille): void
    {
        $this->famille = $famille;
    }





}