<?php

namespace App\Entity;

class Commentaire
{

    protected $personne;
    protected $message;

    /**
     * @return Personne
     */
    public function getPersonne():Personne
    {
        return $this->personne;
    }

    /**
     * @param Personne $personne
     */
    public function setPersonne(Personne $personne): void
    {
        $this->personne = $personne;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage(mixed $message): void
    {
        $this->message = $message;
    }



}