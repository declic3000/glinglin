<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * RgpdReponse
 */
#[ORM\Table(name: 'rgpd_reponse')]
#[ORM\Entity]
class RgpdReponse extends Entity
{

    use TimestampableEntity;


    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;


    /**
     *
     * @var null|Contact
     */
    #[ORM\JoinColumn(nullable: false, name: 'id_contact', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: Contact::class, inversedBy: 'rgpdReponses')]
    #[Ignore]
    protected $contact;

    /**
     *
     * @var null|RgpdQuestion
     */
    #[ORM\JoinColumn(nullable: false, name: 'id_rgpd_question', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: RgpdQuestion::class, inversedBy: 'rgpdReponses')]
    #[Ignore]
    protected $rgpdQuestion;



    /**
     * @var string
     */
    #[ORM\Column(name: 'valeur', type: 'text', length: 65535, nullable: true)]
    protected $valeur;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $idRgpdReponse
     */
    public function setIdRgpdReponse(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Contact|null
     */
    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     */
    public function setContact(?Contact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return RgpdQuestion|null
     */
    public function getRgpdQuestion(): ?RgpdQuestion
    {
        return $this->rgpdQuestion;
    }

    /**
     * @param RgpdQuestion|null $rgpdQuestion
     */
    public function setRgpdQuestion(?RgpdQuestion $rgpdQuestion): void
    {
        $this->rgpdQuestion = $rgpdQuestion;
    }

    /**
     * @return string
     */
    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    /**
     * @param string $valeur
     */
    public function setValeur(string $valeur): void
    {
        $this->valeur = $valeur;
    }





}

