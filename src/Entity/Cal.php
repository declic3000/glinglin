<?php

namespace App\Entity;
use App\Service\CaldavService;
use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Sabre\VObject\Reader;


/**
 * Cal
 */
#[ORM\Table(name: 'calevent')]
#[ORM\Index(name: 'cal_ui', columns: ['uid'])]
#[ORM\Entity]
class Cal extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'uid', type: 'string', length: 120, nullable: false)]
    protected $uid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'agenda', type: 'string', length: 120, nullable: false)]
    protected $agenda;


    /**
     * @var string
     */
    #[ORM\Column(name: 'serveur', type: 'string', length: 120, nullable: false)]
    protected $serveur;



    /**
     * @var string
     *
     */
    #[ORM\Column(name: 'cal', type: 'text', length: 65535, nullable: false)]
    protected $cal;



    /**
     * @var \DateTime
     *
     */
    #[ORM\Column(name: 'date_debut', type: 'datetime', nullable: true)]
    protected $dateDebut;

    /**
     * @var \DateTime
     *
     */
    #[ORM\Column(name: 'date_fin', type: 'datetime', nullable: true)]
    protected $dateFin;


    /**
     * @var ?string
     *
     */
    #[ORM\Column(name: 'lieu', type: 'text', nullable: true)]
    protected $lieu;

    /**
     * @var ?int
     *
     */
    #[ORM\Column(name: 'statut', type: 'integer', nullable: false, options: ['default' => 1])]
    protected $statut = 1;


    /**
     * @var int
     *
     */
    #[ORM\Column(name: 'confidentialite', type: 'integer', nullable: false, options: ['default' => 2])]
    protected $confidentialite = 2;



    /**
     * @var string|null
     *
     */
    #[ORM\Column(name: 'personnes', type: 'text', length: 65535, nullable: true)]
    protected $personnes;



    /**
     * @var int|null
     *
     */
    #[ORM\Column(name: 'nb_personne', type: 'integer', nullable: true)]
    protected $nbPersonne;


    /**
     * @var string|null
     *
     */
    #[ORM\Column(name: 'presentation', type: 'text', length: 65535, nullable: true)]
    protected $presentation;



    /**
     * @var string|null
     *
     */
    #[ORM\Column(name: 'commentaires', type: 'text', length: 65535, nullable: true)]
    protected $commentaires;



    /**
     * Champs pour savoir si des modifications sont à reporter sur l'agenda externe
     * @var bool
     *
     */
    #[ORM\Column(name: 'a_reporter', type: 'boolean')]
    protected $aReporter=false;




    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getAgenda(): string
    {
        return $this->agenda;
    }

    /**
     * @param string $agenda
     */
    public function setAgenda(string $agenda): void
    {
        $this->agenda = $agenda;
    }

    /**
     * @return string
     */
    public function getCal(): string
    {
        return $this->cal;
    }

    /**
     * @param string $cal
     */
    public function setCal(string $cal): void
    {
        $this->cal = $cal;
    }





    /**
     * @return bool
     */
    public function getAReporter() :bool
    {
        return $this->aReporter;
    }

    /**
     * @param bool $jour_a_reporter
     */
    public function setAReporter($aReporter): void
    {
        $this->aReporter = $aReporter;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut(): \DateTime
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut(\DateTime $dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin(): \DateTime
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin(\DateTime $dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(?string $lieu): void
    {
        $this->lieu = $lieu;
    }

    public function getStatut(): ?int
    {
        return $this->statut;
    }

    public function setStatut(?int $statut): void
    {
        $this->statut = $statut;
    }

    public function getConfidentialite(): int
    {
        return $this->confidentialite;
    }

    public function setConfidentialite(int $confidentialite): void
    {
        $this->confidentialite = $confidentialite;
    }

    public function getNbPersonne(): ?int
    {
        return $this->nbPersonne;
    }

    public function setNbPersonne(?int $nbPersonne): void
    {
        $this->nbPersonne = $nbPersonne;
    }






    /**
     * @return string
     */
    public function getServeur(): string
    {
        return $this->serveur;
    }

    /**
     * @param string $serveur
     */
    public function setServeur(string $serveur): void
    {
        $this->serveur = $serveur;
    }

    /**
     * @return string|null
     */
    public function getPersonnes(): ?string
    {
        return $this->personnes;
    }

    /**
     * @param string|null $personnes
     */
    public function setPersonnes(?string $personnes): void
    {
        $this->personnes = $personnes;
    }

    /**
     * @param array|null $personnes
     */
    public function setPersonnesJson(?array $personnes): void
    {
        $this->personnes = json_encode($personnes);
    }

    public function getPersonnesJson(): array
    {
        if($this->personnes){
            return json_decode($this->personnes??'[]',true);
        }
        return [];

    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): void
    {
        $this->presentation = $presentation;
    }





    /**
     * @return string|null
     */
    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    /**
     * @param string|null $commentaires
     */
    public function setCommentaires(?string $commentaires): void
    {
        $this->commentaires = $commentaires;
    }









    /**
     * @param string $nom
     * @param string $message
     */
    public function addCommentaire($nom,$message): void
    {
        $tab = $this->getCommentairesJson();
        $tab[]= [$nom,$message];
        $this->setCommentairesJson($tab);
    }

    /**
     * @param array|null $commentaires
     */
    public function setCommentairesJson(?array $commentaires): void
    {
        $this->commentaires = json_encode($commentaires);
    }

    public function getCommentairesJson(): array
    {
        return json_decode($this->commentaires??'[]',true);
    }


    public function toExport(): array
    {
        $cal = $this->getCal();
        $vcalendar = Reader::read($cal, Reader::OPTION_FORGIVING);
        foreach ($vcalendar->VEVENT->getIterator() as $evt) {
            $e = CaldavService::cal2evt($this,$evt);
        }
        $organisateur =  $e->getOrganisateurs();

        return [
            'serveur' => $this->getServeur(),
            'calendrier' => $this->getAgenda(),
            'date' => $e->getDateDebut(),
            'heure_debut' => $e->getDateDebut()->format('h:i'),
            'heure_fin' => $e->getDateFin()->format('h:i'),
            'titre' => $e->getTitre(),
            'categorie' => $e->getCategories(),
            'lieu' => $this->getLieu(),
            'confidentialite' => $this->getConfidentialite(),
            'status' => $this->getStatut(),
            'couleur' => $e->getCouleur(),
            'organisateur' => isset($organisateur[0])?$e->getOrganisateurs()[0]->getNom():'',
            'organisateur_email' => isset($organisateur[0])?$organisateur[0]->getEmail():'',
            'observation' => $e->getDescriptif(),
            'nb_personne' => $this->getNbPersonne(),
            'presentation' => $this->getPresentation()
        ];
    }




}