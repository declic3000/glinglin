<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * RgpdQuestion
 */
#[ORM\Table(name: 'rgpd_question')]
#[ORM\Entity]
class RgpdQuestion extends Entity
{

    use TimestampableEntity;


    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 250, nullable: true)]
    protected $nom='';


    /**
     * @var string
     */
    #[ORM\Column(name: 'nomcourt', type: 'string', length: 40, nullable: true)]
    protected $nomcourt='';

    /**
     * @var string
     */
    #[ORM\Column(name: 'texte', type: 'text', length: 65535, nullable: true)]
    protected $texte='';


    /**
     * @var integer
     */
    #[ORM\Column(name: 'type', type: 'integer', nullable: false)]
    protected $type = 1;

    /**
     * @var string
     */
    #[ORM\Column(name: 'valeurs', type: 'text', length: 65535, nullable: true)]
    protected $valeurs;

    /**
     * @var string
     */
    #[ORM\Column(name: 'traitements', type: 'text', length: 65535, nullable: true)]
    protected $traitements;


    #[ORM\OneToMany(targetEntity: RgpdReponse::class, mappedBy: 'rgpdQuestion', cascade: ['all'])]
    protected $rgpdReponses;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getNomcourt(): string
    {
        return $this->nomcourt;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt(string $nomcourt): void
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @return string
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * @param string $texte
     */
    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getValeurs(): ?string
    {
        return $this->valeurs;
    }

    /**
     * @param string $valeurs
     */
    public function setValeurs(string $valeurs): void
    {
        $this->valeurs = $valeurs;
    }

    /**
     * @return string
     */
    public function getTraitements(): ?string
    {
        return $this->traitements;
    }

    /**
     * @param string $traitements
     */
    public function setTraitements(string $traitements): void
    {
        $this->traitements = $traitements;
    }





}

