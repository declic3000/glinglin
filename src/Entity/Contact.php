<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * lieu
 */
#[ORM\Table(name: 'contact')]
#[ORM\Index(name: 'contact_nom', columns: ['nom'])]
#[ORM\Entity]
class Contact extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;


    /**
     * @var string
     */
    #[ORM\Column(name: 'cle', type: 'string', length: 240, nullable: false)]
    protected string $cle;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 120, nullable: false)]
    protected string $nom;


    /**
     * @var string
     */
    #[ORM\Column(name: 'email', type: 'string', length: 120, nullable: true)]
    protected string $email;


    /**
     * @var ?string
     */
    #[ORM\Column(name: 'telephone', type: 'string', length: 15, nullable: true)]
    protected ?string $telephone;



    /**
     * Many Contact have Many caracte.
     * @var ArrayCollection
     */
    #[ORM\JoinTable(name: 'contact_caracteristique')]
    #[ORM\JoinColumn(name: 'id_contact', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'id_caracteristique', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: Caracteristique::class)]
    #[Ignore]
    protected $caracteristiques;



    /**
     * Many Contact have Many reponse.
     * @var ArrayCollection
     */
    #[ORM\OneToMany(targetEntity: RgpdReponse::class, mappedBy: 'contact', cascade: ['all'])]
    protected $rgpdReponses;



    public function __construct()
    {
        $this->caracteristiques = new ArrayCollection();
        $this->rgpdReponses = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getCle(): string
    {
        return $this->cle;
    }

    public function setCle(string $cle): void
    {
        $this->cle = $cle;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    public function getCaracteristiques()
    {
        return $this->caracteristiques;
    }

    public function getCaracteristiquesId(): array
    {
        $tab_id=[];
        foreach ($this->getCaracteristiques() as $car){
            $tab_id[] = $car->getId();
        }
        return $tab_id;
    }
    public function getCaracteristiquesSymbole(): string
    {
        $tab_symbole=[];
        foreach ($this->getCaracteristiques() as $car){
            $tab_symbole[] = $car->getSymbole();
        }
        return implode(' ',$tab_symbole);
    }


    public function setCaracteristiques(ArrayCollection $caracteristiques): void
    {
        $this->caracteristiques = $caracteristiques;
    }

    public function addCaracteristique(Caracteristique $caracteristique): void
    {
        if (!$this->caracteristiques->contains($caracteristique)){
            $this->caracteristiques->add($caracteristique);
        }
    }

    public function removeCaracteristique(Caracteristique $caracteristique): void
    {
        if (!$this->caracteristiques->contains($caracteristique)){
            $this->caracteristiques->removeElement($caracteristique);
        }
    }


    public function changeCaracteristique(array $tab_caracteristique): void
    {
        $this->caracteristiques->clear();
        foreach($tab_caracteristique as $caracteristique){
            $this->addCaracteristique($caracteristique);
        }

    }

    public function getRgpdReponses()
    {
        return $this->rgpdReponses;
    }

    public function getRgpdReponsesValeurs(): array
    {
        $tab=[];
        foreach ($this->getRgpdReponses() as $rgpdRepons){
            $tab[$rgpdRepons->getRgpdQuestion()->getId()] = $rgpdRepons->getValeur();
        }
        return $tab;
    }

    public function setRgpdReponses(ArrayCollection $rgpdReponses): void
    {
        $this->rgpdReponses = $rgpdReponses;
    }


}