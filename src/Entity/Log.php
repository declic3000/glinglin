<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Log
 */
#[ORM\Table(name: 'logs')]
#[ORM\Index(name: 'code', columns: ['code'])]
#[ORM\Index(name: 'date_operation', columns: ['date_operation'])]
#[ORM\Entity]
class Log extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id_log', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $idLog;


    /**
     * @var string
     */
    #[ORM\Column(name: 'utilisateur', type: 'string', length: 50, nullable: true)]
    protected $utilisateur;

    

    /**
     * @var string
     */
    #[ORM\Column(name: 'code', type: 'string', length: 9, nullable: false)]
    protected $code;

    /**
     * @var string
     */
    #[ORM\Column(name: 'variables', type: 'text', length: 65535, nullable: false)]
    protected $variables;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_operation', type: 'datetime', nullable: false)]
    protected $dateOperation;

    /**
     * @var string
     */
    #[ORM\Column(name: 'observation', type: 'text', length: 65535, nullable: true)]
    protected $observation;












    /**
     * @return int
     */
    public function getIdLog(): int
    {
        return $this->idLog;
    }

    /**
     * @param int $idLog
     */
    public function setIdLog(int $idLog)
    {
        $this->idLog = $idLog;
    }






    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return json_decode($this->variables, true);
    }

    /**
     * @param array $variables
     */
    public function setVariables($variables)
    {
        $this->variables = json_encode($variables);
    }


    /**
     * @param array $variables
     * @return string
     */
    public function getVariablesEnLigne()
    {
        $tab = $this->getVariables();
        if (is_array($tab)) {
            foreach ($tab as &$t) {
                if (is_array($t)) {
                    $t = implode(', ', $t);
                }
            }
        }
        return $tab;
    }


    /**
     * @return \DateTime
     */
    public function getDateOperation(): \DateTime
    {
        return $this->dateOperation;
    }

    /**
     * @param \DateTime $dateOperation
     */
    public function setDateOperation(\DateTime $dateOperation)
    {
        $this->dateOperation = $dateOperation;
    }

    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getUtilisateur(): ?string
    {
        return $this->utilisateur;
    }

    /**
     * @param string $utilisateur
     */
    public function setUtilisateur(string $utilisateur): void
    {
        $this->utilisateur = $utilisateur;
    }


}

