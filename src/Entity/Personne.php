<?php

namespace App\Entity;

class Personne
{

    protected $email;
    protected $nom;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(mixed $email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(mixed $nom): void
    {
        $this->nom = $nom;
    }


    /**
     * @param string $value
     */
    public function setNomEmail($value): void
    {
        if (is_string($value)){
            if (!str_contains($value,':')){

            }
            else{
                [$this->nom, $this->email] = explode(':',$value);
            }
        }
        elseif(is_array($value)){
            $this->nom = $value['nom'];
            $this->email = $value['email'];

        }

    }


    public function getCle()
    {
        return strtolower(trim($this->nom).':'.trim($this->email));
    }


}