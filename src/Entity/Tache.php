<?php

namespace App\Entity;

class Tache
{

    /**
     * @var integer
     */
    protected $id;


    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $agenda;


    /**
     * @var string
     */
    protected $serveur;

    /**
     * @var string
     */
    protected $titre;
    /**
     * @var string
     */
    protected $descriptif;
    /**
     * @var \DateTime
     */
    protected $date_debut;
    /**
     * @var \DateTime
     */
    protected $date_fin;

    /**
     * @var array|null
     */
    protected $participants;
    /**
     * @var array|null
     */
    protected $organisateurs;



    /**
     * @var string
     */
    protected $priorite="";


    /**
     * @var string
     */
    protected $avancement="";

    /**
     * @var bool
     */
    protected $fini=false;

    /**
     * @var string
     */
    protected $lieu="";

    /**
     * @var string
     */
    protected $adresse="";

    /**
     * @var string
     */
    protected $groupe="";


    /**
     * @var array
     */
    protected $categories = null;


    /**
     * @var string
     */
    protected $statut="";

    /**
     * @var string
     */
    protected $couleur="";


    /**
     * @var array
     */
    protected $commentaires=[];







    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }






    /**
     * @return string
     */
    public function getDescriptif(): string
    {
        return $this->descriptif;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif(string $descriptif): void
    {
        $this->descriptif = $descriptif;


    }

    /**
     * @return array
     */
    public function getCommentaires(): array
    {
        return $this->commentaires;
    }

    /**
     * @param array $commentaires
     */
    public function setCommentaires(array $commentaires): void
    {
        $this->commentaires = $commentaires;
    }






    /**
     * @return null|\DateTime
     */
    public function getDateDebut(): ?\DateTime
    {
        return $this->date_debut;
    }

    /**
     * @param \DateTime $date_debut
     */
    public function setDateDebut(\DateTime $date_debut): void
    {
        $this->date_debut = $date_debut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin(): \DateTime
    {
        return $this->date_fin;
    }

    /**
     * @param \DateTime $date_fin
     */
    public function setDateFin(\DateTime $date_fin): void
    {
        $this->date_fin = $date_fin;
    }


    /**
     * @return array|null
     */
    public function getParticipantsTab(): ?array
    {
        $temp =[];
        foreach($this->participants as $p){
            $temp[]=$p->getNom().':'.$p->getEmail();
        }
        return $temp;
    }



    /**
     * @return array|null
     */
    public function getParticipants(): ?array
    {
        return $this->participants;
    }

    /**
     * @param array|null $participants
     */
    public function setParticipants(?array $participants): void
    {
        $this->participants = $participants;
    }




    /**
     * @return array|null
     */
    public function getOrganisateursTab(): ?array
    {
        $temp =[];
        foreach($this->organisateurs as $p){
            $temp[]=$p->getNom().':'.$p->getEmail();
        }
        return $temp;
    }

    /**
     * @return array|null
     */
    public function getOrganisateurs(): ?array
    {
        return $this->organisateurs;
    }

    /**
     * @param array|null $organisateurs
     */
    public function setOrganisateurs(?array $organisateurs): void
    {
        $this->organisateurs = $organisateurs;
    }






    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getAgenda(): string
    {
        return $this->agenda;
    }

    /**
     * @param string $agenda
     */
    public function setAgenda(string $agenda): void
    {
        $this->agenda = $agenda;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getServeur(): string
    {
        return $this->serveur;
    }

    /**
     * @param string $serveur
     */
    public function setServeur(string $serveur): void
    {
        $this->serveur = $serveur;
    }

    /**
     * @return string
     */
    public function getGroupe(): string
    {
        return $this->groupe;
    }

    /**
     * @param string $groupe
     */
    public function setGroupe(string $groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return string
     */
    public function getCategories(): ?array
    {
        return $this->categories;
    }

    /**
     * @param string $categories
     */
    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }


    /**
     * @return string
     */
    public function getStatut(): string
    {
        return $this->statut;
    }

    /**
     * @return string
     */
    public function getStatutInFrench(): string
    {
        $tab_trad=[
          'COMPLETED' => 'Terminé',
          'IN-PROCESS' => 'En cours',
        ];
        return $tab_trad[$this->statut] ?? $this->statut;
    }


    /**
     * @param string $statut
     */
    public function setStatut(string $statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @param string $couleur
     */
    public function setCouleur(string $couleur): void
    {
        $this->couleur = $couleur;
    }


    public function prioriteInFrench(): string
    {

        $tab_trad=[
            '0' => '??',
            '1' => 'Extrement urgent',
            '2' => 'Trés urgent',
            '3' => 'Urgent',
            '4' => 'Priorité élevé',
            '5' => 'moyenne +',
            '6' => 'moyenne',
            '7' => 'moyenne -',
            '8' => 'faible',
            '9' => 'Peut attendre',
        ];
        return $tab_trad[$this->priorite] ?? $this->priorite;
    }


    public function getPriorite(): string
    {
        return $this->priorite;
    }

    public function setPriorite(string $priorite): void
    {
        $this->priorite = $priorite;
    }



    public function getAvancement(): string
    {
        return $this->avancement;
    }

    public function setAvancement(string $avancement): void
    {
        $this->avancement = $avancement;
    }

    public function isFini(): bool
    {
        return $this->fini;
    }



    public function setFini(bool $fini): void
    {
        $this->fini = $fini;
    }




}