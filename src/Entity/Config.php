<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Config
 */
#[ORM\Table(name: 'configs')]
#[ORM\UniqueConstraint(name: 'configunique', columns: ['nom'])]
#[ORM\Entity]
class Config extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id_config', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $idConfig;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 30, nullable: false)]
    protected $nom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'complement', type: 'string', length: 30, nullable: true)]
    protected $complement;

    /**
     * @var string
     */
    #[ORM\Column(name: 'variables', type: 'text', nullable: true)]
    protected $variables;

    /**
     * @var string
     */
    #[ORM\Column(name: 'observation', type: 'text', nullable: true)]
    protected $observation;





    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());

    }




    /**
     * @return int
     */
    public function getIdConfig(): int
    {
        return $this->idConfig;
    }

    /**
     * @param int $idConfig
     */
    public function setIdConfig(int $idConfig)
    {
        $this->idConfig = $idConfig;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getVariables(): string
    {
        return $this->variables;
    }

    /**
     * @param string $variables
     */
    public function setVariables(string $variables)
    {
        $this->variables = $variables;
    }

    
    /**
     *
     * @return string
     */
    public function getValeur()
    {
        if (is_string($this->variables))
            return json_decode($this->variables, true);
            return '';
    }
    
    /**
     *
     * @param string $variables
     */
    public function setValeur($variables)
    {
        $this->variables = json_encode($variables);
    }
    
    
    
    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }




}

