<?php

namespace App\Enum;

use \Spatie\IcalendarGenerator\Enums\Classification as ClassificationEnum;
enum Classification: int {
    use EnumToArray;
    case PUBLIC = 2;
    case PRIVATE = 1;
    case CONFIDENTIAL = 0;

    public static function eventClassification($key): ClassificationEnum
    {
        switch ($key){
            case 0:
                return ClassificationEnum::confidential();
            case 1:
                return ClassificationEnum::private();
            case 2:
                return ClassificationEnum::public();
        }
    }
}
