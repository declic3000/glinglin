<?php

namespace App\Enum;
use Spatie\IcalendarGenerator\Enums\EventStatus;

enum Status: int {
    use EnumToArray;
    case TENTATIVE = 2;
    case CONFIRMED = 1;
    case CANCELLED = 0;


    public static function eventStatus($key): EventStatus
    {
        switch ($key) {
            case 0:
                return \Spatie\IcalendarGenerator\Enums\EventStatus::cancelled();
            case 1:
                return \Spatie\IcalendarGenerator\Enums\EventStatus::confirmed();
            case 2:
                return \Spatie\IcalendarGenerator\Enums\EventStatus::tentative();
        }
        return \Spatie\IcalendarGenerator\Enums\EventStatus::confirmed();
    }



}

