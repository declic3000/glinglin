<?php

namespace App\Command;

use App\Service\CaldavService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Cache\CacheInterface;

class SynchroCommand extends Command
{
    protected static $defaultName = 'app:synchro';

    protected $caldavService;
    protected $cacheCool;

    public function __construct(CaldavService $caldavService,CacheInterface $cacheCool)
    {
        $this->caldavService = $caldavService;
        $this->cacheCool = $cacheCool;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Synchroniser l\'agenda avec la base de données');
        $this->addOption('ecraser_pc','a',InputOption::VALUE_NONE,'Mettre à jour les champs personnes et commentaire depuis le serveur' );
    }



    protected function execute(InputInterface $input, OutputInterface $output) :int
    {
        $io = new SymfonyStyle($input, $output);
        $option_ecraser = $input->getOption('ecraser_pc');
        $tab_serveur = $this->caldavService->getListeServeur();
        $date_debut = (new \DateTime())->sub(new \DateInterval('P4D'));
        $date_fin = (new \DateTime())->add(new \DateInterval('P6M'));
        foreach ($tab_serveur as $serveur){

            $this->caldavService->envoyerModificationAuServeurDistant($serveur);
            $this->caldavService->rechargeEvtDepuisServeurDistant($date_debut,$date_fin,$serveur,$option_ecraser);
        }
        $io->success('Synchro OK');
        return 0;
    }
}
