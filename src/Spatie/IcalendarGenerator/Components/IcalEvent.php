<?php
namespace App\Spatie\IcalendarGenerator\Components;

use Spatie\IcalendarGenerator\Components\Event;
use Spatie\IcalendarGenerator\ComponentPayload;
use Spatie\IcalendarGenerator\Properties\TextProperty;


class IcalEvent extends Event
{
    public static function create(string $name = null): IcalEvent
    {
        return new self($name);
    }

    public ?string $descriptionHtml = null;
    public ?string $color = null;
    public ?array $categories = null;

    public function descriptionHtml(string $descriptionHtml): IcalEvent
    {
        $this->descriptionHtml = $descriptionHtml;
        return $this;
    }

    public function categories(array $categories): IcalEvent
    {
        $this->categories = $categories;
        return $this;
    }

    public function color(string $color): IcalEvent
    {
        $this->color = $color;
        return $this;
    }

    protected function payload(): ComponentPayload
    {
        $payload = parent::payload();
        $this->resolveCustomProperties($payload);
        return $payload;
    }


    private function resolveCustomProperties(ComponentPayload $payload): self
    {
        $payload
            ->optional(
                $this->descriptionHtml,
                fn () => TextProperty::create('X-ALT-DESC;FMTTYPE=TEXT/HTML', $this->descriptionHtml)
            )
            ->optional(
                $this->categories,
                fn () => TextProperty::create('CATEGORIES', implode(", ", $this->categories))->withoutEscaping()
            )
            ->optional(
                $this->color,
                fn () => TextProperty::create('COLOR',  $this->color)->withoutEscaping()
            );
        return $this;
    }
}