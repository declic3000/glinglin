<?php


namespace App\Form;



use App\Entity\Zonegroupe;
use App\Enum\Classification;
use App\Enum\Status;
use App\Service\CaldavService;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvtType extends AbstractType
{
 protected $tab_categorie;
 protected $tab_lieu;
 protected $tab_serveur;



    protected $router;
        function __construct(Sac $sac,CaldavService $caldavService)
    {
        $this->tab_lieu = table_colonne_cle_valeur($sac->tab('lieu'),'nom','nom');
        $this->tab_categorie = table_colonne_cle_valeur($sac->tab('categorie'),'nom','nom');
        $this->tab_serveur=$caldavService->getListeServeurCalendrier(true);

    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $tab_serveur = array_keys($this->tab_serveur);
        if (count($this->tab_serveur)==1){
            $builder->add('serveur', HiddenType::CLASS,['empty_data'=>array_pop($tab_serveur)]);
        }
        else{
            $builder->add('serveur', ChoiceType::CLASS,['choices'=>array_combine($tab_serveur,$this->tab_serveur)]);
        }

        $tab_calendrier = [];
        foreach($this->tab_serveur as $serveur => $t_calendrier){
            foreach($t_calendrier as $calendrier => $calendrier_info){
                if (!($calendrier_info['read_only']??false)){
                    $titre = $calendrier_info['title']??$calendrier;
                    $tab_calendrier[$titre.''] = $calendrier;
                }
            }
        }

        if (count($tab_calendrier)==1){
            $builder->add('calendrier', HiddenType::CLASS,['empty_data'=>array_pop($tab_calendrier)]);
        }
        else{
            $builder->add('calendrier', ChoiceType::CLASS,['choices'=>$tab_calendrier]);
        }


        $builder->add('date', DateType::class, [
                'label' => 'Date',
                'required' => true,
                'widget' => 'single_text'
            ])
            ->add('heure_debut', TextType::class, ['attr'=>[ 'class' => 'timepicker']])
            ->add('heure_fin', TextType::class, ['attr'=>[ 'class' => 'timepicker']])
            ->add('titre', TextType::class, ['attr'=>[ 'class' => ''],'required'=>true])
            ->add('categorie_add',HiddenType::class,['mapped'=>false])
            ->add('categorie',ChoiceType::class, [
                    'required' => false,
                    'attr' => [
                        'class' => 'autocomplete_select2',
                        'data-message' => 'Aucune catégorie trouvée',
                        'data-tags' => 1,
                        'data-cible' => 'input#addEvt_categorie_add',
                        'placeholder' => '',
                    ],
                    'choices' => $this->tab_categorie,
                    'multiple' => true
                ])
            ->add('nb_personne',NumberType::class,[])
            ->add('lieu_add',HiddenType::class,['mapped'=>false])
            ->add('lieu', ChoiceType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'autocomplete_select2',
                    'data-message' => 'Aucun lieu trouvée',
                    'data-tags' => 1,
                    'data-cible' => 'input#addEvt_lieu_add',
                    'placeholder' => '',
                ],
                'choices' => $this->tab_lieu,
                'multiple' => false
            ])
               ->add('confidentialite', ChoiceType::class, [
                   'required' => false,
                   'attr' => [],
                   'choices' => Classification::arrayflip(),
                   'data'=> 2,
                   'multiple' => false
               ])
                ->add('status', ChoiceType::class, [
                    'required' => false,
                    'attr' => [],
                    'choices' => Status::arrayflip(),
                    'data'=> 1,
                    'multiple' => false
                ])
               ->add('couleur', ColorType::class, ['attr'=>[ 'class' => '']])
               ->add('organisateur', TextType::class, ['attr'=>[ 'class' => 'secondaire']])
               ->add('organisateur_email', TextType::class, ['attr'=>[ 'class' => 'secondaire']])
               ->add('observation', TextareaType::class, ['attr'=>[ 'class' => 'secondaire']])
               ->add('presentation', TextareaType::class, ['attr'=>[ 'class' => 'secondaire']]);
    //    $builder->setDataMapper($this);
    }




}

