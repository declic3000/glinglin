<?php


namespace App\Form;



use App\Enum\Classification;
use App\Enum\Status;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Zonegroupe;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EvenementLotEvtType extends AbstractType
{
 protected $tab_lieu;
 protected $tab_categorie;

    function __construct(Sac $sac)
    {
        $this->tab_lieu = table_colonne_cle_valeur($sac->tab('lieu'),'nom','nom');
        $this->tab_categorie = table_colonne_cle_valeur($sac->tab('categorie'),'nom','nom');
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $jour_de_la_semaine = [
            '0' => 'Lundi',
            '1' => 'Mardi',
            '2' => 'Mercredi',
            '3' => 'Jeudi',
            '4' => 'Vendredi',
            '5' => 'Samedi',
            '6' => 'Dimanche'
        ];

        $builder->add('jour_semaine', ChoiceType::class, ['label' => 'Jour de semaine', 'expanded' => false, 'choices' => array_flip($jour_de_la_semaine), 'required' => true])
               ->add('titre', TextType::class, ['attr'=>[ 'class' => '']])
            ->add('categorie_add',HiddenType::class,['mapped'=>false])
            ->add('categorie',ChoiceType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'autocomplete_select2',
                    'data-message' => 'Aucune catégorie trouvée',
                    'data-tags' => 1,
                    'data-cible' => 'input#addEvt_categorie_add',
                    'placeholder' => '',
                ],
                'choices' => $this->tab_categorie,
                'multiple' => true
            ])
            ->add('nb_personne',NumberType::class,[])
            ->add('lieu_add',HiddenType::class,['mapped'=>false])
            ->add('lieu', ChoiceType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'autocomplete_select2',
                    'data-message' => 'Aucun lieu trouvée',
                    'data-tags' => 1,
                    'data-cible' => 'input#addEvt_lieu_add',
                    'placeholder' => '',
                ],
                'choices' => $this->tab_lieu,
                'multiple' => false
            ])
            ->add('confidentialite', ChoiceType::class, [
                'required' => false,
                'attr' => [],
                'choices' => Classification::arrayflip(),
                'multiple' => false
            ])
            ->add('status', ChoiceType::class, [
                'required' => false,
                'attr' => [],
                'choices' => Status::arrayflip(),
                'multiple' => false
            ])
               ->add('couleur', ColorType::class, ['attr'=>[ 'class' => '']])
               ->add('heure_debut', TextType::class, ['attr'=>[ 'class' => 'timepicker']])
               ->add('heure_fin', TextType::class, ['attr'=>[ 'class' => 'timepicker']])
               ->add('organisateur', TextType::class, ['attr'=>[ 'class' => 'secondaire']])
               ->add('organisateur_email', TextType::class, ['attr'=>[ 'class' => 'secondaire']])
               ->add('observation', TextareaType::class, ['attr'=>[ 'class' => 'secondaire']])
                ->add('presentation', TextareaType::class, ['attr'=>[ 'class' => 'secondaire']]);

    }

}