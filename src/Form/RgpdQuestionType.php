<?php
namespace App\Form;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

class RgpdQuestionType extends AbstractType
{

    protected $prestationType;


    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('nom', TextType::class, ['constraints' => new Assert\NotBlank(), 'attr' => []])
            ->add('nomcourt', TextType::class, ['constraints' => new Assert\NotBlank(), 'attr' => []])
            ->add('texte', TextareaType::class, ['attr' => []]
    );
    }


}