<?php


namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Zonegroupe;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CaracteristiqueType extends AbstractType
{





    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

           $builder->add('nom', TextType::CLASS,['attr' => ['class'=>'']]);
           $builder->add('symbole', TextType::CLASS,['attr' => ['class'=>'']]);
           $builder->add('descriptif', TextareaType::CLASS,['attr' => ['class'=>'']]);
           $builder->add('famille', TextType::CLASS,['attr' => ['class'=>'']]);

    }

}