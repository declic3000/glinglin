<?php


namespace App\Form;



use App\Service\CaldavService;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Zonegroupe;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AddEvtsType extends AbstractType
{


    protected $tab_permanence;
    protected $tab_serveur;

    function __construct(Sac $sac,CaldavService $caldavService)
    {
        $tab_permanence  = $sac->conf('permanences');
        if (!empty($tab_permanence)){
            foreach ($tab_permanence as &$p){
                $p['nom'] .=  '('.count($p['evts']??[]).' evts)';
            }
        }
        $this->tab_permanence = table_simplifier($tab_permanence,'nom');
        $this->tab_serveur=$caldavService->getListeServeurCalendrier(true);

    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $tab_semaine = range(1,52);
        $tab_semaine = array_flip($tab_semaine);
        $date_du_jour = new \DateTime();
        [$date, $date_debut, $date_fin] = donneDateSemaine($date_du_jour->format('Y').'-01-01');
        if ($date->format('W')>1){
            [$date, $date_debut, $date_fin] = donneDateSemaine($date_du_jour->format('Y').'-01-05');
        }
        foreach($tab_semaine as $id=>&$s){

            [$date, $date_debut, $date_fin] = donneDateSemaine($date->format('Y-m-d'));
            $s = 'Semaine '.$id .' ('.$date_debut->format('d M').' au '.$date_fin->format('d M').')';
            $date->add(new \DateInterval('P7D'));
        }

        if (count($this->tab_serveur)==1){
            $builder->add('serveur', HiddenType::CLASS);
        }
        else{
            $builder->add('serveur', ChoiceType::CLASS,['choices'=>array_combine(array_keys($this->tab_serveur),$this->tab_serveur)]);
        }

        $tab_calendrier = [];

        foreach($this->tab_serveur as $serveur => $t_calendrier){
            foreach($t_calendrier as $calendrier => $calendrier_info){
                if (!($calendrier_info['read_only']??false)){
                    $titre = $calendrier_info['title']??$calendrier;
                    $tab_calendrier[$titre.''] = $calendrier;
                }
            }
        }

        if (count($tab_calendrier)==1){
            $builder->add('calendrier', HiddenType::CLASS);
        }
        else{
            $builder->add('calendrier', ChoiceType::CLASS,['choices'=>$tab_calendrier]);
        }


           $builder->add('annee', TextType::CLASS,['attr' => ['class'=>'']])
               ->add('semaine', ChoiceType::class, ['label' => 'Semaine', 'expanded' => false, 'choices' => array_flip($tab_semaine), 'required' => true])
               ->add('indice_lot', ChoiceType::class, ['label' => 'Lot de permanence', 'expanded' => false, 'choices' => array_flip($this->tab_permanence), 'required' => true]);
    }

}