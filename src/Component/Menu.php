<?php

namespace App\Component;


use Declic3000\Pelican\Event\MenuModifEvent;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Menu
{

    protected $sac;
    protected $dispatcher;


    function __construct(Sac $sac, EventDispatcherInterface $eventDispatcher)
    {
        $this->sac = $sac;
        $this->dispatcher = $eventDispatcher;
    }


    function get()
    {
        $menu = [];
        return $menu;
    }
}
