<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class ContactTable extends Table
{

    protected $objet = 'contact';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['title' => 'Nom du contact'],
        'email' => ['title' => 'Email'],
        'action' => []
    ];



}
