<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class RgpdQuestionTable extends Table
{

    protected $objet = 'rgpd_question';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'nomcourt' => [],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];





}
