<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class CaracteristiqueTable extends Table
{

    protected $objet = 'caracteristique';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'symbole' => ['title' => 'symbole'],
        'nom' => ['title' => 'Nom de la caracteristique'],
        'famille' => ['title' => 'Famille'],
        'action' => []
    ];



}
