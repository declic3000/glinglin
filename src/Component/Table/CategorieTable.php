<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class CategorieTable extends Table
{

    protected $objet = 'categorie';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['title' => 'Nom de la catégorie'],
        'action' => []
    ];



}
