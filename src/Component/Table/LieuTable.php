<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class LieuTable extends Table
{

    protected $objet = 'lieu';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['title' => 'Nom du lieu'],
        'action' => []
    ];



}
