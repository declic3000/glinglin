<?php

namespace App\Service;


use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Cookie;


class CookieService
{

    public function __construct(private Requete $requete)
    {
        $this->requete = $requete;
    }

    public function get($nom, $default =null)
    {
        $cookie=$this->requete->getRequest()->cookies;
        return ($cookie->get($nom))??$default;
    }

    public function getJson($nom, $default =[])
    {
        $values = $this->get($nom, '[]');
        return json_decode((string) $values,true);
    }


    public function protectionCle($cle): array|string
    {
        $cle= $this->normaliser(trim($cle));
        list($nom,$email) = explode(':',$cle);
        return $this->sanitize_cle($nom,true,true).':'.$this->sanitize_cle($email,true,true);
    }


    function sanitize_cle($string, $force_lowercase = true, $anal = false)
    {
        $strip = ["~", "`", "!", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]", "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;", "â€”", "â€“", ",", "<", ">", "/", "?"];
        $clean = trim(str_replace($strip, "", strip_tags((string)$string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9.@]/", "", (string)$clean) : $clean;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower((string)$clean, 'UTF-8') :
                strtolower((string)$clean) :
            $clean;
    }


    function normaliser($str)
    {
        $charMap = [
            'a' => '/[àáâã]/iu',
            'c' => '/ç/iu',
            'e' => '/[èéêë]/iu',
            'i' => '/[ïíî]/iu',
            'o' => '/[ôó]/iu',
            'oe' => '/œ/iu',
            'u' => '/[üúû]/iu'
        ];
        $keys = array_keys($charMap);
        $values = array_values($charMap);
        return preg_replace($values, $keys, (string)$str);
    }






    public function getIdentite(): ?array
    {
        $value = $this->getJson('glinglin');
        if (!empty($value)) {
            return $value;
        }
        return null;
    }

    public function getCleIdentite()
    {
        $value = $this->getIdentite();
        if (isset($value['cle'])) {
            return $value['cle'];
        }
        return null;
    }


    public function cookie($nom,$data=[],$time=3600){
        return new Cookie(
            $nom,	// Cookie name.
            json_encode($data),	// Cookie value.
            time() + ( $time)	// Expires 2 years.
        );

    }

}
