<?php

namespace App\Service;


use App\Entity\Cal;
use App\Entity\CalTodo;
use App\Entity\Commentaire;
use App\Entity\Evenement;
use App\Entity\Organisateur;
use App\Entity\Participant;
use App\Entity\Personne;
use App\Entity\Tache;
use App\Enum\Classification;
use App\Enum\Status;
use App\Spatie\IcalendarGenerator\Components\IcalEvent;
use Doctrine\Persistence\ManagerRegistry;
use it\thecsea\simple_caldav_client\CalDAVFilter;
use it\thecsea\simple_caldav_client\SimpleCalDAVClient;
use Spatie\IcalendarGenerator\Components\Calendar;
use Sabre\VObject;


class CaldavService
{
    private $client;
    private $em;


    public function __construct(private array $settings, ManagerRegistry $managerRegistry)
    {
        $this->client = new SimpleCalDAVClient();
        $this->em = $managerRegistry->getManager();
    }

    public function getListeServeur(?bool $writable =null) :array{
        $tab_serveur = $this->settings;
        foreach($tab_serveur as $serveur=>&$conf){
            $conf['type'] ??= 'nextcloud';
            switch ($conf['type']){
                case 'ics':
                    if ($writable !==true ){
                    $tab[]= $serveur;
                    }
                    break;
                default: // nextcloud
                    foreach($conf['calendriers']  as $calendrier=>$calendrier_info) {
                        if ($writable === null or $writable == !($calendrier_info['read_only'] ?? false)) {
                            $tab[]= $serveur;
                        }
                    }
                    break;
            }
        }
        return array_unique($tab);

    }

    public function getListeServeurCalendrier(?bool $writable =null) :array{
        $tab =[];
        $ics_indice = 0;
        $tab_serveur = $this->settings;
        foreach($tab_serveur as $serveur=>&$conf){
            $conf['type'] ??= 'nextcloud';
            switch ($conf['type']){
                case 'ics':
                    if (!$writable){
                        $tab[$serveur]['agenda_ics'.$ics_indice] = $conf;
                        $ics_indice ++;
                    }
                break;
                default: // nextcloud
                    foreach($conf['calendriers']  as $calendrier=>$calendrier_info) {
                        if ($writable === null or $writable == !($calendrier_info['read_only'] ?? false)) {
                            $calendrier_info['type']='nextcloud';
                            $tab[$serveur][$calendrier] = $calendrier_info;
                        }
                    }
                break;
            }


        }
        return $tab;
    }

    public function envoyerModificationAuServeurDistant($serveur='agenda_ca'){
        $conf = $this->settings[$serveur];
        $tab_evt_local_o = $this->em->getRepository(Cal::class)->findBy(['aReporter'=>true]);
        $tab_calendrier = $this->getListeServeurCalendrier(true);
        $tab_evt_local=[];
        foreach($tab_evt_local_o as $evt) {
            if (isset($tab_calendrier[$evt->getAgenda()])){
                $tab_evt_local[$evt->getAgenda()][$evt->getUid()]=$evt;
            }
        }

        foreach($tab_evt_local as $agenda=>$tab_cal){
            foreach($tab_cal as $uid=>$cal) {
                $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
                $arrayOfCalendars = $this->client->findCalendars();
                if (isset($arrayOfCalendars[$agenda])){
                    $this->client->setCalendar($arrayOfCalendars[$agenda]);
                    $ok_modif = $this->modifierEvt($uid,$cal);
                    if ($ok_modif) {
                        $cal->setAReporter(false);
                        $this->em->persist($cal);
                        $this->em->flush();
                    }
                }
            }




        }


    }



    function modifierEvt($uid,$cal) :bool{


        $filter = new CalDAVFilter("VEVENT");
        $filter->mustIncludeMatchSubstr("UID", $uid, false);
        $events = $this->client->getCustomReport($filter->toXML());
        $event = array_shift($events);
        if ($event) {
            $href = $event->getHref();
            $etag = $event->getEtag();
            try {
                $this->client->change($href, $cal->getCal(), $etag);
            } catch (\Exception) {
                return false;
            }
            return true;
        }
        return false;
    }






    public function rechargeTodoDepuisServeurDistant($serveur=null,$ecraser_pc=false){


        if ($serveur){
            $tab_serveur=[$serveur];
        }
        else{
            $tab_serveur = $this->getListeServeur();
        }

        foreach($tab_serveur as $serveur){
            $conf = $this->settings[$serveur];
            $tab_agenda = $conf['calendriers'];
            $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
            $arrayOfCalendars = $this->client->findCalendars(); // Returns an array of all accessible calendars on the server.

            $tab_evt_local_o = $this->em->getRepository(CalTodo::class)->findBy(['serveur'=>$serveur]);
            $tab_evt_local = [];
            foreach($tab_evt_local_o as $evt){
                $tab_evt_local[$evt->getAgenda().'---'.$evt->getUid()]= $evt;
            }

            $evts = '';
            $tab=[];
            foreach ($tab_agenda as $nom_agenda=>$agenda_info) {
                $this->client->setCalendar($arrayOfCalendars[$nom_agenda]);
                $events = $this->client->getTODOs();
                $tab[$nom_agenda] = $events;
            }



            $tab_cle=[];
            foreach ($tab as $nom_agenda=>$events) {

                foreach ($events as $evts) {
                    $data = $evts->getData();

                    $vcalendar = VObject\Reader::read($data, VObject\Reader::OPTION_FORGIVING);
                    foreach ($vcalendar->VTODO->getIterator() as $evt) {
                        $uid = $evt->UID->getValue();
                        $date_debut = null;
                        if($evt->DTSTART){
                            $date_debut = $this->transformeDate($evt->DTSTART->getValue());
                        }
                        $date_fin = null;
                        if($evt->DTEND){
                            $date_fin = $this->transformeDate($evt->DTEND->getValue());
                        }

                        $cle = $nom_agenda.'---'.$uid;
                        $date_modif_dist = date_create_from_format('Ymd\THis\Z',$evt->DTSTAMP->getValue());

                        if(isset($tab_evt_local[$cle])){
                            $tab_cle[] = $cle;
                            $e = $tab_evt_local[$cle];

                            //if ($date_modif_dist > $e->getUpdatedAt()){
                            $e->setCal($data);
                            if($date_debut!=null){
                                $e->setDateDebut($date_debut);
                            }
                            if($date_fin!=null){
                                $e->setDateFin($date_fin);
                            }
                            if($date_modif_dist!=null) {
                                $e->setUpdatedAt($date_modif_dist);
                            }
                            else{
                                $e->setUpdatedAt(new \DateTime());
                            }
                            if ($ecraser_pc){
                                $e->setCommentairesJson($this->extraireCommentaires($evt));
                                $e->setPersonnesJson( $this->extrairePersonnes($evt));
                            }

                            $this->em->persist($e);
                            //}
                        } else {
                            $tab_cle[] = $cle;
                            $e = new CalTodo();
                            $e->setUid($uid);
                            $e->setServeur($serveur);
                            $e->setAgenda($nom_agenda);
                            if($date_debut!=null) {
                                $e->setDateDebut($date_debut);
                            }
                            if($date_fin!=null) {
                                $e->setDateFin($date_fin);
                            }
                            $e->setCal($data);
                            if ($ecraser_pc) {
                                $e->setCommentairesJson($this->extraireCommentaires($evt));
                                $e->setPersonnesJson($this->extrairePersonnes($evt));
                            }
                            $e->setCreatedAt(new \DateTime());
                            $e->setUpdatedAt(new \DateTime());
                            $this->em->persist($e);
                        }

                    }

                }
            }

            $this->em->flush();



            $tab_evt_local_o = $this->em->getRepository(Cal::class)->findBy(['serveur'=>$serveur]);
            $tab_evt_local=[];
            foreach($tab_evt_local_o as $evt) {
                $cle = $evt->getAgenda().'---'.$evt->getUid();
                $tab_evt_local[$cle]= $evt;
                if(!in_array($cle,$tab_cle)){
                    $this->em->remove($evt);
                }
            }
            $this->em->flush();
        }



    }



    public function recupICS($url,$date_debut,$date_fin)
    {
        $session_curl = curl_init($url);
        curl_setopt($session_curl , CURLOPT_HEADER, false);
        curl_setopt ($session_curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($session_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session_curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');
        $page_content = curl_exec($session_curl);
        curl_close($session_curl);
        $vcalendar = VObject\Reader::read($page_content);
        return $vcalendar;
    }



    public function rechargeEvtDepuisServeurDistant(\DateTime $date_debut, \DateTime $date_fin,$serveur=null,$ecraser_pc=false){

        $this->em->getRepository(Cal::class)->findAll();
        if ($serveur){
            $tab_serveur=[$serveur];
        }
        else{
            $tab_serveur = $this->getListeServeur();
        }

        foreach($tab_serveur as $serveur){
            $conf = $this->settings[$serveur];

            $type=$conf['type']??'nextcloud';
            $ind_ics=0;
            switch($type){
                case 'ics':
                    $events =  $this->recupICS($conf['url'],$date_debut->format('Ymd\THis\Z'), $date_fin->format('Ymd\THis\Z'));
                    $tab['agenda_ics'.$ind_ics] = $events;
                    $ind_ics++;
                    break;
                default:
                    $tab_agenda = $conf['calendriers'];
                    $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
                    $arrayOfCalendars = $this->client->findCalendars(); // Returns an array of all accessible calendars on the server.
                    $tab=[];
                    foreach ($tab_agenda as $nom_agenda => $agenda_info) {
                        $this->client->setCalendar($arrayOfCalendars[$nom_agenda]);
                        $events = $this->client->getEvents($date_debut->format('Ymd\THis\Z'), $date_fin->format('Ymd\THis\Z'));
                        foreach($events as &$ex){
                            $data= $ex->getData();
                            $ex = VObject\Reader::read($data, VObject\Reader::OPTION_FORGIVING);
                        }
                        $tab[$nom_agenda] = $events;

                    }
                    break;

            }



            $tab_evt_local_o = $this->em->getRepository(Cal::class)->findBy(['serveur'=>$serveur]);
            $tab_evt_local = [];
            foreach($tab_evt_local_o as $evt_local){
                $tab_evt_local[$evt_local->getAgenda().'---'.$evt_local->getUid()]= $evt_local;
            }




            $tab_cle=[];

            foreach ($tab as $nom_agenda=>$events) {

                foreach ($events as $evts) {

                    foreach ($evts->VEVENT->getIterator() as $evt) {



                        $data = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Sabre//Sabre VObject 4.0.0-beta1//EN
CALSCALE:GREGORIAN
'.$evt->serialize().'
END:VCALENDAR';
                        $uid = $evt->UID->getValue();
                        $date_debut = $this->transformeDate($evt->DTSTART->getValue());
                        $date_fin = $this->transformeDate($evt->DTEND->getValue());
                        $cle = $nom_agenda.'---'.$uid;
                        $date_modif_dist = date_create_from_format('Ymd\THis\Z',$evt->DTSTAMP->getValue());
                        if(isset($tab_evt_local[$cle])){
                            $tab_cle[] = $cle;
                            $e = $tab_evt_local[$cle];

                            if ($data!==$e->getCal()){

                            //if ($date_modif_dist > $e->getUpdatedAt()){
                                $e->setCal($data);
                                $e->setDateDebut($date_debut);
                                $e->setDateFin($date_fin);
                                if ($date_modif_dist){
                                    $e->setUpdatedAt($date_modif_dist);
                                }
                                if ($ecraser_pc){
                                    $e->setCommentairesJson($this->extraireCommentaires($evt));
                                    $e->setPersonnesJson( $this->extrairePersonnes($evt));
                                }
                                $e->setUpdatedAt(new \DateTime());
                                $this->em->persist($e);
                            }
                            //}
                        } else {
                            $tab_cle[] = $cle;
                            $e = new Cal();
                            $e->setUid($uid);
                            $e->setServeur($serveur);
                            $e->setAgenda($nom_agenda);
                            $e->setDateDebut($date_debut);
                            $e->setDateFin($date_fin);
                            $e->setCal($data);
                            if ($ecraser_pc) {
                                $e->setCommentairesJson($this->extraireCommentaires($evt));
                                $e->setPersonnesJson($this->extrairePersonnes($evt));
                            }
                            $e->setCreatedAt(new \DateTime());
                            $e->setUpdatedAt(new \DateTime());
                            $this->em->persist($e);
                        }

                    }

                }
            }

            $this->em->flush();
            $tab_evt_local_o = $this->em->getRepository(Cal::class)->findBy(['serveur'=>$serveur]);
            $tab_evt_local=[];
            foreach($tab_evt_local_o as $evt) {
                $cle = $evt->getAgenda().'---'.$evt->getUid();
                $tab_evt_local[$cle]= $evt;
                if(!in_array($cle,$tab_cle)){
                    $this->em->remove($evt);
                }
            }
            $this->em->flush();
        }
    }


    function extraireCommentaires($evt)
    {
        $description=$evt->DESCRIPTION ?: '';
        $tab_commentaire=[];
        $r = preg_match('`^(.*)----------- COMMENTAIRES -----------(.*)$`ms', $description, $matches);
        if ($r > 0) {
            $commentaires = explode("\n-----\n", $matches[2]);
            foreach ($commentaires as $c) {
                $pos = strpos($c, " :\n");
                $personne = '';
                if ($pos === false) {
                    $message = $c;
                } else {
                    $personne = trim(substr($c, 0, $pos));
                    $message = trim(substr($c, $pos + 3));
                }
                $tab_commentaire[] = [$personne, $message];
            }
        }
        return $tab_commentaire;
    }

    function extrairePersonnes($evt){

        $attendees = $evt->ATTENDEE;
        $tab_attendee_temp = [];
        if (!empty($attendees)) {
            foreach ($attendees as $attendee) {
                $nom = $attendee->parameters()['CN'] ? $attendee->parameters()['CN']->getValue() : '';
                $email = substr((string) $attendee->getValue(), 7);
                $tab_attendee_temp[] = $nom.':'.$email;
            }
        }
        return $tab_attendee_temp;
    }

    public static function cal2evt(Cal $cal,$evt): ?Evenement
    {
        $uid = $evt->UID->getValue();
        if ($evt->DTSTART!==null){
            $date_debut = CaldavService::transformeDate($evt->DTSTART->getValue());
            $evt_tmp = (new Evenement());
            $evt_tmp->setId($cal->getId());
            $evt_tmp->setAgenda($cal->getAgenda());
            $evt_tmp->setServeur($cal->getServeur());
            $evt_tmp->setUid($uid);
            $evt_tmp->setTitre($evt->SUMMARY ? $evt->SUMMARY->getValue() : '');
            $evt_tmp->setDateDebut($date_debut);
            $evt_tmp->setDateFin(CaldavService::transformeDate($evt->DTEND->getValue()));
            $description = $evt->DESCRIPTION ?: '';
            $evt_tmp->setDescriptif(CaldavService::extraireDescriptif($description));
            $evt_tmp->setNbPersonne(CaldavService::extraireNbPersonne($description));
            $evt_tmp->setPresentation(CaldavService::extrairePresentation($description));
            $tab_commentaire = [];
            foreach($cal->getCommentairesJson() as $commentaire){
                $p = new Personne();
                $p->setNomEmail($commentaire[0]);
                $c = new Commentaire();
                $c->setPersonne($p);
                $c->setMessage($commentaire[1]);
                $tab_commentaire[]=$c;
            }
            $evt_tmp->setCommentaires($tab_commentaire);
            $evt_tmp->setStatut($evt->STATUS ?: '');
            $evt_tmp->setCouleur($evt->COLOR ?: '');
            if ($evt->CATEGORIES){
                $categories =  explode(',',(string) $evt->CATEGORIES->getValue()) ;
                $evt_tmp->setCategories(is_array($categories)?$categories:[$categories]);
            }
            $evt_tmp->setLieu($evt->LOCATION ? $evt->LOCATION->getValue() : '');
            $evt_tmp->setAdresse($evt->ADDRESS ? $evt->ADDRESS->getValue() : '');
            $attendees = $cal->getPersonnesJson();
            $tab_attendee_temp = [];
            if (!empty($attendees)) {
                foreach ($attendees as $attendee) {
                    $p = new Participant();
                    $p->setNomEmail($attendee);
                    $tab_attendee_temp[] = $p;
                }
            }
            $evt_tmp->setParticipants($tab_attendee_temp);
            $organizers = $evt->ORGANIZER;
            $tab_organizer_temp = [];
            if (!empty($organizers)) {
                foreach ($organizers as $organizer) {
                    $o = new Organisateur();
                    $nom =(isset($organizer->parameters()['CN']) ) ? $organizer->parameters()['CN']->getValue() : '';
                    if (empty($nom)){
                        $nom =$organizer->getValue();
                    }
                    $o->setNom($nom);
                    $email = (str_contains((string) $organizer->getValue(),'@')) ? substr((string) $organizer->getValue(),7):'';
                    if (empty($email)){
                        $email =(isset($organizer->parameters()['SENT-BY']) ) ? substr((string) $organizer->parameters()['SENT-BY']->getValue(),7) : '';
                    }
                    $o->setEmail($email);
                    $tab_organizer_temp[] = $o;
                }
            }
            $evt_tmp->setOrganisateurs($tab_organizer_temp);
            return $evt_tmp;
            }
        return null;
    }



    public function transformeCal2Evt($ecal) :array
    {
        $tab_evt_local=[];
        $cal = $ecal->getCal();
        $vcalendar = VObject\Reader::read($cal, VObject\Reader::OPTION_FORGIVING);
        foreach ($vcalendar->VEVENT->getIterator() as $evt) {
            $e = CaldavService::cal2evt($ecal,$evt);
            $tab_evt_local[date_timestamp_get($e->getDateDebut()).'---'.$e->getUid()]= $e;
        }
        return $tab_evt_local;
    }


    public function getEvt($id)
    {
        $ecal = $this->em->getRepository(Cal::class)->find($id);
        $tab_evt = $this->transformeCal2Evt($ecal);
        return array_shift($tab_evt);
    }


    public function getEvtGroupe($id)
    {
        $evt = $this->getEvt($id);
        $date_debut =$evt->getDateDebut();
        $nb_groupe = preg_match('`^\[[^\]]*\]`ui',(string) $evt->getTitre(),$output);
        $groupe='';
        if ($nb_groupe==1){
            $groupe=$output[0];
        }
        $tab_evt =  $this->getEvtsGroupe($date_debut, $date_debut);

        $code_jour = $evt->getDateDebut()->format('Ymd');

        if (isset($tab_evt[$code_jour.'-'.$groupe])){
            return $tab_evt[$code_jour.'-'.$groupe];
        }elseif (isset($tab_evt[$code_jour.'-'.$evt->getUid()])){

           return $tab_evt[$code_jour.'-'.$evt->getUid()];

        }


    }



    public function getEvts(\DateTime $date_debut, \DateTime $date_fin,$force_maj=false,$tab_filtres=null)
    {
        if ($force_maj){
            $this->rechargeEvtDepuisServeurDistant($date_debut,$date_fin);
        }
        $db=$this->em->getConnection();
        $where_suppl='';
        if ($tab_filtres){
            if (!empty( $tab_filtres['calendriers'])){
                $tab_filtre_calendriers= explode('|',(string)$tab_filtres['calendriers']);
                $where_suppl_cal=[];
                foreach ($tab_filtre_calendriers as &$filte_calendrier){
                    [$serveur, $agenda]=explode(':',$filte_calendrier);
                    $where_suppl_cal[] = '( serveur='.$db->quote($serveur).' and agenda = '.$db->quote($agenda).')';
                }
                $where_suppl .= ' AND ('.implode(' OR ',$where_suppl_cal).')';
            }
            if (!empty($tab_filtres['public'])) {
                $where_suppl .= ' AND confidentialite=2';
            }
        }




        $tab_evt_id = $db->fetchFirstColumn('select id from calevent where '.
            'date_debut>='.$db->quote($date_debut->format('Y-m-d').' 00:00:00').
            ' and date_fin<='.$db->quote($date_fin->format('Y-m-d'.' 23:59:59')).
            $where_suppl );
        $tab_evt_local_o = $this->em->getRepository(Cal::class)->findBy(['id'=>$tab_evt_id],['dateDebut'=>'ASC']);
        $tab_evt_local = [];
        foreach($tab_evt_local_o as $ecal){
            $cal = $ecal->getCal();
            $vcalendar = VObject\Reader::read($cal, VObject\Reader::OPTION_FORGIVING);
            foreach ($vcalendar->VEVENT->getIterator() as $evt) {
                $e = CaldavService::cal2evt($ecal,$evt);
                if ($e){
                    $tab_evt_local[date_timestamp_get($e->getDateDebut()).'---'.$e->getUid()]= $e;
                }
                else {

                }
            }
        }
        return $tab_evt_local;
    }


    public function getEvtsGroupe(\DateTime $date_debut, \DateTime $date_fin,$force_maj=false,$tab_filtres=null)
    {
        $tab_evt =  $this->getEvts($date_debut, $date_fin,$force_maj,$tab_filtres);
        $tab_evt_g=[];
        foreach($tab_evt as $evt){
            $code_jour = $evt->getDateDebut()->format('Ymd');
            $groupe = preg_match('`^\[[^\]]*\]`ui',(string) $evt->getTitre(),$output);
            if ($groupe == 1){
                $groupe=$output[0];
            }
            else{
                $groupe = $evt->getUid();
            }
            if (!isset($tab_evt_g[$code_jour.'-'.$groupe])){
                $tab_evt_g[$code_jour.'-'.$groupe]['groupe']=$groupe;
                $tab_evt_g[$code_jour.'-'.$groupe]['titre']= trim(trim((string) $groupe,'[]'));
                $tab_evt_g[$code_jour.'-'.$groupe]['dateDebut']=$evt->getDateDebut();
                if (!isset($tab_evt_g[$code_jour.'-'.$groupe]['categorie'])){
                    $tab_evt_g[$code_jour.'-'.$groupe]['categorie']=[];
                }
                $tab_cat = $evt->getCategories();
                if ($tab_cat===null){
                    $tab_cat =[];
                }
                $tab_evt_g[$code_jour.'-'.$groupe]['categorie']=array_merge($tab_evt_g[$code_jour.'-'.$groupe]['categorie'],$tab_cat);
                if (!isset($tab_evt_g[$code_jour.'-'.$groupe]['lieux'])){
                    $tab_evt_g[$code_jour.'-'.$groupe]['lieux']=[];
                }
                if (!empty($evt->getLieu())){
                    $tab_evt_g[$code_jour.'-'.$groupe]['lieux'][]=$evt->getLieu();
                }
            }
            $tab_evt_g[$code_jour.'-'.$groupe]['categorie'] = array_unique($tab_evt_g[$code_jour.'-'.$groupe]['categorie']??[]);
            $tab_evt_g[$code_jour.'-'.$groupe]['lieux'] = array_unique($tab_evt_g[$code_jour.'-'.$groupe]['lieux']??[]);
            $tab_evt_g[$code_jour.'-'.$groupe]['dateFin']=$evt->getDateFin();
            $tab_evt_g[$code_jour.'-'.$groupe]['evts'][]=$evt;
        }
        return $tab_evt_g;
    }


    public static function transformeDate(string $date): \DateTime|bool
    {
        if (strlen($date)===8){
            return date_create_from_format('Ymd',$date);
        }
        else {
            $datetime = date_create_from_format('Ymd\THis',$date);
            if ($datetime){
                return $datetime;
            } else {
                return \DateTime::createFromFormat('Ymd\THis\Z',$date);
            }
        }
    }





    public function getTaches(?bool $fini = null,$force_maj=false)
    {
        if ($force_maj){
            $this->rechargeTodoDepuisServeurDistant();
        }
        $tab_todoo =  $this->em->getRepository(CalTodo::class)->findAll();
        $tab_todo = [];
        foreach($tab_todoo as $ecal){
            $cal = $ecal->getCal();
            $vcalendar = VObject\Reader::read($cal, VObject\Reader::OPTION_FORGIVING);
            foreach ($vcalendar->VTODO->getIterator() as $evt) {
                $e = $this->cal2todo($ecal,$evt);
                $tab_todo[$e->getUid()]= $e;
            }
        }
        return $tab_todo;
    }



    public function getTODOS(?string $serveur=null)
    {
        if ($serveur){
            $tab_serveur=[$serveur];
        }
        else{
            $tab_serveur = $this->getListeServeur();
        }
        foreach($tab_serveur as $serveur) {
            $conf = $this->settings[$serveur];
            $tab_agenda = $conf["calendriers"];
            $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
            $arrayOfCalendars = $this->client->findCalendars(); // Returns an array of all accessible calendars on the server.
            $todos = [];
            foreach ($tab_agenda as $nom_agenda) {
                $this->client->setCalendar($arrayOfCalendars[$nom_agenda]);

                $events = $this->client->getTODOs();
                foreach ($events as $event) {
                    $todos[] = $this->parseTodos($event->getData());
                }
            }
        }
        return $todos;
    }

    public function addEvt(string $serveur,string $calendrier,string $titre,string $descriptif,string $lieu,\DateTime $date_debut, \DateTime $date_fin,$options=[],$force_maj=false)
    {
        $cal_evt = $this->cal_event($titre,$descriptif,$lieu,$date_debut,$date_fin);
        if (isset($options['organisateur']) && !empty($options['organisateur'])){
            $cal_evt->organizer($options['organisateur_email'],$options['organisateur']);
        }
        if (isset($options['categorie']) && !empty($options['categorie'])){
            $cal_evt->categories($options['categorie']);
        }
        if (isset($options['couleur']) && !empty($options['couleur'])){
            $cal_evt->color(rechercherCouleur($options['couleur']));
        }
        if (isset($options['confidentialite']) && !empty($options['confidentialite'])){
            $cal_evt->classification(Classification::eventClassification($options['confidentialite']));
        }
        if (isset($options['status'])){
            $cal_evt->status(Status::eventStatus($options['status']));
        }

        $cal = Calendar::create($calendrier)->event($cal_evt)->toString();
        $conf = $this->settings[$serveur];
        $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
        $arrayOfCalendars = $this->client->findCalendars();
        if (isset($arrayOfCalendars[$calendrier])) {
            $this->client->setCalendar($arrayOfCalendars[$calendrier]);
            return $this->client->create($cal);
        }
    }


    public function majCal(Cal $cal,$datas){
        $chaine = $cal->getCal();
        $vcalendar = VObject\Reader::read($chaine, VObject\Reader::OPTION_FORGIVING);
        $tab_evt = $vcalendar->VEVENT->getIterator();
        $cal_evt = $tab_evt->current();
        $cal_evt->SUMMARY=$datas['titre'];
        $description = $this->construireDescription($datas['observation'],$datas['nb_personne'],$datas['presentation']);
        $cal_evt->DESCRIPTION = $description;


        $cal->setLieu($datas['lieu']);
        $cal->setNbPersonne($datas['nb_personne']);
        $cal->setPresentation($datas['presentation']);

        /*if (isset($options['organisateur']) && !empty($options['organisateur'])){
            $cal_evt->organizer($options['organisateur_email'],$options['organisateur']);
        }
        if (isset($options['categorie']) && !empty($options['categorie'])){
            $cal_evt->categories($options['categorie']);
        }
        if (isset($options['couleur']) && !empty($options['couleur'])){
            $cal_evt->color(rechercherCouleur($options['couleur']));
        }
        if (isset($options['confidentialite']) && !empty($options['confidentialite'])){
            $cal_evt->classification(Classification::eventClassification($options['confidentialite']));
        }
        */
        if (isset($datas['status'])){
            $cal_evt->STATUS = Status::eventStatus($datas['status']);
            $cal->setStatut($datas['status']);
        }
        $cal->setCal($vcalendar->serialize());
        return $cal;
    }

    public function syncEvt(Cal $cal,$force_maj=false)
    {


        $serveur = $cal->getServeur();
        $agenda = $cal->getAgenda();
        $uid = $cal->getUid();
        $conf = $this->settings[$serveur];
        if ( ($conf['type']??'')!=='ics' && isset($conf['baseUri'])){
            $this->client->connect($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', $conf['userName'], $conf['password']);
            $arrayOfCalendars = $this->client->findCalendars();
              if (isset($arrayOfCalendars[$agenda])){
                    $this->client->setCalendar($arrayOfCalendars[$agenda]);
                    $ok_modif = $this->modifierEvt($uid,$cal);
                    if ($ok_modif) {
                        $cal->setAReporter(false);
                        $this->em->persist($cal);
                        $this->em->flush();
                        return true;
                    }
                }
        }
        return false;
    }



    function replaceEvent($chaine,$new_chaine){
        $tab = explode("\n",(string) $chaine);
        $debut=null;
        $fin=null;
        foreach($tab as $i=>$line){
            if ($debut ===null  ){
                if ($line==='BEGIN:VEVENT'){
                    $debut=$i;
                }
            }
            else{
                if ($line==='END:VEVENT'){
                    $fin=$i;
                    break;
                }
            }
        }

        return implode("\n",array_slice($tab,0,$debut+1))
            ."\n".$new_chaine."\n".implode("\n",array_slice($tab,$fin));

    }


    function construireDescription($description,$nb_personne,$presentation,$commentaires=[]){
        if ($nb_personne){
            $description .= "\r\n".'NB_PERSONNE: '.$nb_personne;
        }

        if ($presentation){
            $description .= "\r\n".'----------- PRESENTATION -----------'."\r\n".chunk_split($presentation);
        }

        if (count($commentaires)>0){
            $description .= "\r\n".'----------- COMMENTAIRES -----------'."\r\n".chunk_split(implode('\n-----\n',$commentaires));
        }
        return $description;
    }



    function addCalCommentaire(Cal $ecal){

        $chaine = $ecal->getCal();
        $vcalendar = VObject\Reader::read($chaine, VObject\Reader::OPTION_FORGIVING);
        $tab_evt = $vcalendar->VEVENT->getIterator();
        $evt= $tab_evt->current();
        $description = $evt->DESCRIPTION ?: '';
        $descriptif = CaldavService::extraireDescriptif($description);
        $commentaires = [];
        $tab = $ecal->getCommentairesJson();
        $nb_personne = $ecal->getNbPersonne();
        $presentation = $ecal->getPresentation();
        foreach ($tab as $com){
            $commentaires[] = implode(" :\n",$com);
        }

       $chaine_stop =  'END:VEVENT|ATTENDEE|SUMMARY|CATEGORIES|DTSTART|DTEND|ORGANIZER';
        $description = $this->construireDescription($descriptif,$nb_personne,$presentation,$commentaires);


        $re = '/DESCRIPTION;(.*)("data:.*?"):(.*?)\n('.$chaine_stop.')/iums';
        $chaine = preg_replace($re,'$4',$chaine);


        $re = '/DESCRIPTION(.*?)\n('.$chaine_stop.')/iums';

        $chaine = preg_replace($re,'$2',(string) $chaine);

        $description = 'DESCRIPTION:'.str_replace(["\n","\r"],['\n',''],$description);

        $chaine = str_replace('END:VEVENT',$description."\nEND:VEVENT",$chaine);

        $ecal->setCal($chaine);
        return true;
    }

    function addParticipantEvent($chaine,$nom,$email){
        $attendee = 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN='.$nom.':MAILTO:'.$email;
        return str_replace('END:VEVENT',$attendee."\nEND:VEVENT",$chaine);
    }

    function removeParticipantEvent($chaine,$nom,$email){
        $re = '/(ATTENDEE.*)([\r\n]+)[^ATTENDEE]/iu';
        $chaine = preg_replace($re,'$1',(string) $chaine);
        $attendee = '`ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN='.$nom.':MAILTO:'.$email."`ui";
        return preg_replace($attendee,'',$chaine);
    }



    function cal_event($titre,$description,$lieu,$dateDebut,$dateFin){
        $evt = IcalEvent::create($titre)
            ->description($description)
            ->address($lieu)
            ->startsAt($dateDebut)
            ->endsAt($dateFin);
        return $evt;
    }



    function parseTodos($string)
    {
        $keys = [
            'CREATED' => 'cree_le',
            'LAST-MODIFIED' => 'modifie_le',
            'PERCENT-COMPLETE' => 'modifie_le',
            'STATUS' => 'statut',
            'UID' => 'uid',
            'SUMMARY' => "titre",
            'DESCRIPTION' => "description",
            'RELATED-TO' => "relation",
            'PRIORITY' => "priority",
            'CATEGORY' => "categorie",
            'COMPLETED' => "date_cloture",
        ];
        $tab = [];
        $tab_string = explode("\n", (string) $string);
        foreach ($tab_string as $s) {
            if (strpos($s, ':') > -1) {
                [$cle, $valeur] = explode(':', $s);
                if (isset($keys[$cle])) {
                    $tab[$keys[$cle]] = $valeur;
                }
            }
        }
        return $tab;
    }



    /**
     * @param string $description
     */
    public static function extraireDescriptif(string $description): string
    {
        $r = preg_match('`^(.*)----------- (COMMENTAIRES|PRESENTATION) -----------(.*)$`ms',$description,$matches);
        $descriptif='';
        if($r===0){
            $descriptif = $description;
        } else {
            $descriptif = $matches[1];
        }
        $descriptif = preg_replace("`^NB_PERSONNE: [0-9]{1,9}$`ms",'',$descriptif,1);
        return $descriptif;
    }


    /**
     * @param string $description
     */
    public static function extraireNbPersonne(string $description): ?int
    {
        $r = preg_match("`^NB_PERSONNE: ([0-9]{1,9})$`ms",$description,$matches);
        if($r==1){
            return (int)$matches[1];
        }
        return null;
    }

    /**
     * @param string $description
     */
    public static function extrairePresentation(string $description): ?string
    {
        $r = preg_match('`^.*----------- PRESENTATION -----------(((?!----------- COMMENTAIRES -----------).)*)(----------- COMMENTAIRES -----------(.*))?$`ms',$description,$matches);

        if($r>0){
            return trim($matches[1]);
        }
        return null;
    }

    private function cal2todo($cal, $evt)
    {
        $uid = $evt->UID->getValue();
        $evt_tmp = (new Tache());
        $evt_tmp->setId($cal->getId());
        $evt_tmp->setAgenda($cal->getAgenda());
        $evt_tmp->setServeur($cal->getServeur());
        $evt_tmp->setUid($uid);
        $evt_tmp->setTitre($evt->SUMMARY ? $evt->SUMMARY->getValue() : '');
        if($evt->DTSTART){
            $evt_tmp->setDateDebut($this->transformeDate($evt->DTSTART->getValue()));
        }
        if($evt->DTEND) {
            $evt_tmp->setDateFin($this->transformeDate($evt->DTEND->getValue()));
        }
        $description = $evt->DESCRIPTION ?: '';
        $evt_tmp->setDescriptif($this->extraireDescriptif($description));
        $tab_commentaire = [];
        foreach($cal->getCommentairesJson() as $commentaire){
            $p = new Personne();
            $p->setNomEmail($commentaire[0]);
            $c = new Commentaire();
            $c->setPersonne($p);
            $c->setMessage($commentaire[1]);
            $tab_commentaire[]=$c;
        }
        $evt_tmp->setCommentaires($tab_commentaire);


        $evt_tmp->setPriorite($evt->PRIORITY ?: '');
        $evt_tmp->setFini($evt->COMPLETED=="1" ? $evt->COMPLETED : '');
        $namep='PERCENT-COMPLETE';
        $evt_tmp->setAvancement($evt->$namep ?: '');
        $evt_tmp->setStatut($evt->STATUS ?: '');
        $evt_tmp->setCouleur($evt->COLOR ?: '');
        if ($evt->CATEGORIES){
            $categories =  explode(',',(string) $evt->CATEGORIES->getValue()) ;
            $evt_tmp->setCategories(is_array($categories)?$categories:[$categories]);
        }

        $evt_tmp->setLieu($evt->LOCATION ? $evt->LOCATION->getValue() : '');
        $evt_tmp->setAdresse($evt->ADDRESS ? $evt->ADDRESS->getValue() : '');
        $attendees = $cal->getPersonnesJson();
        $tab_attendee_temp = [];


        if (!empty($attendees)) {
            foreach ($attendees as $attendee) {
                $p = new Participant();
                $p->setNomEmail($attendee);
                $tab_attendee_temp[] = $p;
            }
        }
        $evt_tmp->setParticipants($tab_attendee_temp);



        $organizers = $evt->ORGANIZER;
        $tab_organizer_temp = [];

        if (!empty($organizers)) {
            foreach ($organizers as $organizer) {
                $o = new Organisateur();
                $nom =(isset($organizer->parameters()['CN']) ) ? $organizer->parameters()['CN']->getValue() : '';
                if (empty($nom)){
                    $nom =$organizer->getValue();
                }
                $o->setNom($nom);

                $email = (str_contains((string) $organizer->getValue(),'@')) ? substr((string) $organizer->getValue(),7):'';
                if (empty($email)){
                    $email =(isset($organizer->parameters()['SENT-BY']) ) ? substr((string) $organizer->parameters()['SENT-BY']->getValue(),7) : '';

                }
                $o->setEmail($email);
                $tab_organizer_temp[] = $o;
            }
        }
        $evt_tmp->setOrganisateurs($tab_organizer_temp);
        return $evt_tmp;
    }
}
