<?php

namespace App\Service;


use App\Entity\Cal;
use App\Entity\CalTodo;
use App\Entity\Commentaire;
use App\Entity\Evenement;
use App\Entity\Organisateur;
use App\Entity\Participant;
use App\Entity\Personne;
use App\Entity\Tache;
use Doctrine\Persistence\ManagerRegistry;
use it\thecsea\simple_caldav_client\CalDAVFilter;
use it\thecsea\simple_caldav_client\SimpleCalDAVClient;

use App\Spatie\IcalendarGenerator\Components\Calendar;
use App\Spatie\IcalendarGenerator\Components\Event;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Sabre\VObject;


class RaspismsService
{
    private $client;

    public function __construct(private readonly string $url, private readonly string $token)
    {
        $this->client = new CurlHttpClient();
    }


    function envoyerSms(array $numero,string $texte){

        $response = $this->client->request(
            'POST',
            $this->url .'/api/scheduled/',
            [
                'headers' => [
                    'X-Api-Key: '.$this->token,
                    'Accept: application/json',
                ],
                'body' => [
                    'text' => $texte,
                    'numbers' => '+33695334259',
                ]
            ]
        );
        $statusCode = $response->getStatusCode();
        echo($statusCode);
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

        return $content;

    }



//curl -X POST https://app.raspisms.fr/api/scheduled/
// -H 'X-Api-Key: ac122e8d46faac0615c181b93d07612d'
// -d 'text=Mon%20SMS%20d%27exemple'
// -d 'numbers=%2B33612345678'



}
