<?php

namespace App\Service;

use App\Entity\Cal;
use App\Entity\Commentaire;
use App\Entity\Evenement;
use App\Entity\Organisateur;
use App\Entity\Participant;
use App\Entity\Personne;
use Doctrine\Persistence\ManagerRegistry;
use it\thecsea\simple_caldav_client\CalDAVFilter;
use it\thecsea\simple_caldav_client\SimpleCalDAVClient;
use MStilkerich\CardDavClient\Account;
use MStilkerich\CardDavClient\Config;
use MStilkerich\CardDavClient\Services\Discovery;
use MStilkerich\CardDavClient\Services\Sync;
use MStilkerich\CardDavClient\Services\SyncHandler;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Psr\Log\NullLogger;
use Sabre\VObject;
use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;

class StdoutLogger extends AbstractLogger
{
    public function log($level, $message, array $context = []): void
    {
        if ($level !== LogLevel::DEBUG) {
            $ctx = empty($context) ? "" : json_encode($context);
            echo $message . $ctx . "\n";
        }
    }
}


class EchoSyncHandler implements SyncHandler
{
    public function addressObjectChanged(string $uri, string $etag, ?VObject\Component\VCard $card): void
    {
        if (isset($card)) {
            $fn = $card->FN ?? "<no name>";
            echo "   +++ Changed or new card $uri (ETag $etag): $fn\n";
        } else {
            echo "   +++ Changed or new card $uri (ETag $etag): Error: failed to retrieve/parse card's address data\n";
        }
    }

    public function addressObjectDeleted(string $uri): void
    {
        echo "   --- Deleted Card $uri\n";
    }

    public function getExistingVCardETags(): array
    {
        return [];
    }

    public function finalizeSync(): void
    {
    }
}




class CarddavService
{
    private $client;
    private $em;


    public function __construct(private array $settings, ManagerRegistry $managerRegistry)
    {
        $this->em = $managerRegistry->getManager();
    }


    public function getListeServeur() :array
    {
        return array_keys($this->settings);
    }

    public function rechargeDepuisServeurDistant($serveur = null, $ecraser_pc = false)
    {


        if ($serveur) {
            $tab_serveur = [$serveur];
        } else {
            $tab_serveur = $this->getListeServeur();
        }
        $tab_card=[];
        $log = new StdoutLogger();
        $httplog = new NullLogger();
        Config::init($log, $httplog);
        foreach ($tab_serveur as $serveur) {
            $conf = $this->settings[$serveur];
            $account = new Account($conf['baseUri'] . '/principals/users/' . $conf['userName'] . '/', ["username" => $conf['userName'], "password" => $conf['password']]);
            try {
                $discover = new Discovery();
                $abooks = $discover->discoverAddressbooks($account);
            } catch (\Exception $e) {
                dump("!!! Error during addressbook discovery: " . $e->getMessage());
                exit(1);
            }


            $abook = $abooks[0];
            $vcards = $abook->query([], [], true, 1000);
            $tab_card=[];
            foreach ($vcards as $vcard) {
                $card =[
                    'nom'=>$vcard['vcard']->FN->getValue()
                ];
                if (isset($vcard['vcard']->EMAIL)) {
                    $card['email']=$vcard['vcard']->EMAIL->getValue();
                }
                if (isset($vcard['vcard']->TEL)) {
                    if ($vcard['vcard']->TEL->count()>1) {
                        foreach ($vcard['vcard']->TEL as $tel) {
                            $tab_params = $tel->parameters();
                            $type='telephone';
                            if (isset($tab_params['TYPE'])) {
                                switch ($tab_params['TYPE']->getValue()) {
                                    case 'work':
                                        $type ='telephone_pro';
                                        break;
                                    case 'cell':
                                        $type ='mobile';
                                        break;
                                    default:
                                }
                            }
                            if (strlen((string) $tel->getValue())>5) {
                                $card[$type]=$tel->getValue();
                            }
                        }
                    } else {
                        if (strlen((string) $vcard['vcard']->TEL->getValue())>5) {
                            $card['telephone']=$vcard['vcard']->TEL->getValue();
                        }
                    }
                }
                if (isset($vcard['vcard']->ORG)) {
                    $card['societe']=$vcard['vcard']->ORG->getValue();
                }

                if (isset($vcard['vcard']->CATEGORIES)) {
                    if ($vcard['vcard']->CATEGORIES->count()>1) {
                        foreach ($vcard['vcard']->CATEGORIES as $cat) {
                            $card['groupe'] = $cat->getValue();
                        }
                    } else {
                        $card['groupe']=[$vcard['vcard']->CATEGORIES->getValue()];
                    }
                    $card['groupe']=implode(';',$card['groupe']);
                }
                $tab_card[$vcard['vcard']->UID->getValue()]=$card;
               // dump($vcard['vcard']->N->getValue());
            }

        }
        return $tab_card;
    }

}
