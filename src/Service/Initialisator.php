<?php

namespace App\Service;

use App\Entity\Bloc;
use App\Entity\Composition;
use App\Entity\CompositionBloc;
use App\Entity\Courrier;
use App\Entity\Mot;
use App\Entity\Motgroupe;
use App\Entity\Unite;
use Declic3000\Pelican\Service\Chargeur;


class Initialisator extends \Declic3000\Pelican\Service\Initialisator
{


    function init_app(){


        $nb = $this->db->fetchOne("SELECT count(*) FROM sys_configs");
        if ($nb === 0) {
            $this->config_maj();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM sys_preferences");
        if ($nb === 0) {
            $this->preference_maj();
        }


        $nb = $this->db->fetchOne("SELECT count(*) FROM asso_motgroupes");
        if ($nb === 0) {
            $this->initialiser_mot();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM asso_unites");
        if ($nb === 0) {
            $this->initialiser_unite();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM com_blocs");
        if ($nb === 0) {
            $this->initialiser_bloc();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM com_courriers");
        if ($nb === 0) {
            $this->initialiser_courrier();
        }

        $this->sac->initSac(true);
    }



    function verifier_creer_motgroupe($tab_data, $cle = 'nomcourt')
    {

        $this->chargeur = new Chargeur($this->em);
        $mg = $this->chargeur->charger_objet_by('motgroupe', [$cle => $tab_data['nomcourt']]);
        if (empty($mg)) {
            $mg = new Motgroupe();
        }

        $mg->setNom('Système');
        $mg->setNomcourt('systeme');
        $mg->setSysteme(true);
        $mg->setobjetsEnLien('membre;individu');
        $mg->setImportance('1');
        $mg->setParent(null);
        $this->em->persist($mg);
        $this->em->flush();

    }


    function initialiser_mot()
    {


        $tab_groupe = (new \App\Init\VariablesMotGroupe())->getVariables();

        $tab_motgroupo = [];
        foreach ($tab_groupe as $k => $groupe) {

            $mg = $this->chargeur->charger_objet_by('motgroupe', ['nomcourt' => $k]);
            if (empty($mg)) {
                $mg = new Motgroupe();
            } else{
                $mg = $mg[0];
            }
            $mg->setNom($groupe['nom']);
            $mg->setNomcourt($k);
            $mg->setSysteme(true);
            $mg->setobjetsEnLien($groupe['objetEnLien']);
            $mg->setImportance($groupe['importance']);
            if (isset($groupe['parent']))
                $mg->setParent($tab_motgroupo[$groupe['parent']]);
            $this->em->persist($mg);
            $tab_motgroupo[$k] = $mg;

        }

        $this->em->flush();
        $tab_mots = (new \App\Init\VariablesMot())->getVariables();

        foreach ($tab_mots as $k_mg => $tab_mot) {
            foreach ($tab_mot as $k => $nom) {
                $m = $this->chargeur->charger_objet_by('mot', ['nomcourt' => $k]);
                $m = (empty($m)) ? new Mot() : $m[0];
                $m->setNom($nom);
                $m->setNomcourt($k);
                $m->setMotgroupe($tab_motgroupo[$k_mg]);
                $m->setImportance(1);
                $this->em->persist($m);
            }
        }
        $this->em->flush();

        return true;
    }


    function initialiser_unite()
    {

        $tab_unite = [
            'unite' => ['nom' => 'Unité', 'nombre' => '1'],
            'l' => ['nom' => 'Litre', 'nombre' => '2'],
            'm3' => ['nom' => 'Metre cube', 'nombre' => '2']
        ];


        foreach ($tab_unite as $k => $nom) {
            $m = $this->chargeur->charger_objet_by('unite', ['nomcourt' => $k]);
            $m = (empty($m)) ? new Unite() : $m[0];
            $m->setNom($nom['nom']);
            $m->setNomcourt($k);
            $m->setNombre((int)$nom['nombre']);
            $m->setActif(true);
            $this->em->persist($m);
        }

        $this->em->flush();

        return true;
    }


    function initialiser_bloc()
    {
        $tab_canal = getBlocUsage();
        $tab_bloc = [];
        foreach ($tab_canal as $c => $canal) {
            $rep = __DIR__ . '/../../documents/' . $canal . '/';
            foreach (glob($rep . "*.html.twig") as $f) {
                $nom = substr(basename($f), 0, -10);
                $bloc = $this->em->getRepository(Bloc::class)->findOneBy(['nom' => $nom, 'canal' => $c]);
                if (!$bloc) {
                    $bloc = new Bloc();
                    $bloc->setNom($nom);
                    $bloc->setCanal($c);
                }
                $bloc->setTexte(file_get_contents($f));
                $this->em->persist($bloc);
                $tab_bloc[$c][$nom] = $bloc;
            }
        }
        $this->em->flush();
        $tab_composition = (new \App\Init\VariablesBloc())->getVariables();
        foreach ($tab_composition as $comp) {

            $composition = $this->em->getRepository(Composition::class)->findOneBy(['nom' => $comp['nom']]);
            if (!$composition) {
                $composition = new Composition();
                $composition->setNom($comp['nom']);
                $composition->setNomcourt($comp['nomcourt']);
                $composition->setCategorie($comp['categorie'] ?? $comp['nom']);
                $this->em->persist($composition);
                $this->em->flush();

                foreach ($tab_canal as $c => $canal) {
                    if (isset($comp['canal'][$c])) {
                        foreach ($comp['canal'][$c] as $k => $nom_b) {
                            $compositionbloc = new CompositionBloc();
                            if ($c === 'L') {
                                $compositionbloc->setPositionFeuille('corp');
                            }
                            if (is_array($nom_b)) {
                                $compositionbloc->setBloc($tab_bloc[$c][$k]);
                                if (isset($nom_b['css'])) {
                                    $compositionbloc->setCss($nom_b['css']);
                                }
                                if (isset($nom_b['position_feuille'])) {
                                    $compositionbloc->setPositionFeuille($nom_b['position_feuille']);
                                }
                                $compositionbloc->setBloc($tab_bloc[$c][$k]);
                            } else {
                                $compositionbloc->setBloc($tab_bloc[$c][$nom_b]);
                            }
                            $compositionbloc->setComposition($composition);
                            $this->em->persist($compositionbloc);
                        }
                    }

                }

            }
        }
        $this->em->flush();
        return true;

    }


    function initialiser_courrier()
    {
        $tab_courrier = (new \App\Init\VariablesCourrier())->getVariables();
        foreach ($tab_courrier as $data) {

            $courrier = new Courrier();
            $courrier->fromArray($data);
            $tab_comp = $this->chargeur->charger_table('com_compositions');
            $tab_comp = table_colonne_cle_valeur($tab_comp, 'nomcourt', 'id_composition');
            $tab_cbloc = $this->em->getRepository(CompositionBloc::class)->findBy(['composition' => $tab_comp[$data['composition']]]);
            $valeur = [];
            $tab_canal = getCourrierCanal();

            foreach ($tab_cbloc as $cbloc) {
                $bloc = $cbloc->getBloc();
                $tab_vars = [];
                //$tab_vars = $documentator->document_rechercher_variables($bloc->getTexte());
                $valeur[$tab_canal[$bloc->getCanal()]][$bloc->getNom()] = [
                    'texte' => $bloc->getTexte(),
                    'variables' => $tab_vars,
                    'css' => $bloc->getCss(),
                    'position' => 'corp',
                    'ordre' => 0
                ];
            }
            $comp = $this->em->getRepository(Composition::class)->findOneBy(['nomcourt' => $data['composition']]);
            $courrier->setComposition($comp);
            $courrier->setValeur($valeur);
            $this->em->persist($courrier);
            $this->em->flush();
        }
    }


}


