<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariableInterface;

class VariablesConfig extends VariableInterface
{
    function getVariables()
    {
        return [
            'general' => [
                'variables' => [
                    'organisation' => '',
                    'slogan' => '',
                    'explicatif' => [
                        'type_champs' => 'textarea',
                        'valeur' =>''
                    ],
                    'pays' => 'FR',
                    'langue' => 'fr',
                ]
            ],

            'page_publique' => [
                'variables' => [
                    'active' => false,
                    'titre' => '',
                    'texte' => [
                        'type_champs' => 'textarea',
                        'valeur' =>''
                    ]
                ]
            ],


            'categorie' => [
                'variables' => [
                    'desactiver_inscription'  => [
                        'type_champs' => 'categorie',
                        'valeur' => 0
                    ]
                ]
            ],


            'module' => [
                'variables' => [
                    'sms' => false,
                    'caracteristique' => false,
                    'rgpd' => false,
                    'export_spip' => false,
                ]
            ],

            'affichage' => [
                'variables' => [
                    'mode_affichage'=>[
                        'type_champs' => 'radio',
                        'choices' => array_flip(getModeAffichage()),
                        'valeur' => 'calendrier'
                    ],
                    'compact' => false,
                    'filtre' => false,
                    'minicalendrier' => true,
                    'boutons_liens' => true,
                    'couleur_du_fond' => [
                        'valeur'=>'#dadada',
                        'type_champs'=>'couleur'
                        ]
                    ]
                ],
            'logo' => [
                'variables' => [
                    'hauteur_max'=>120,
                    'largeur_max'=>120,
                    'logo_perso' => [
                        'type_champs'=>'fichier_asset',
                        'nom_destination'=>'logo_perso.png'
                    ]
                ]
            ]
        ];
    }
}
