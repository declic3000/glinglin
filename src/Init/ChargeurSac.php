<?php


namespace App\Init;

use Declic3000\Pelican\Init\ChargeurSacInterface;

class ChargeurSac extends ChargeurSacInterface
{


    // /////////////////////////////////////////////////////////////////////////////////
    // // Fichier qui regroupe les fonctions pour travailler sur la super variable "table"
    function chargement_table():array
    {
        $tab_a_charger = [
            'categorie',
            'lieu',
            'caracteristique',
            'rgpd_question'
        ];

        $tab=[];
        foreach ($tab_a_charger as $table) {
            $tab[$table] = $this->charger_table($table);
        }

        return $tab;
    }


}