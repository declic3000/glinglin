<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariablePreferenceInterface;

class VariablesPreference extends VariablePreferenceInterface
{
    function getVariables()
    {

        return [
            /////////////////////////////
            /// TIMELINE
            ////////////////////////////

            'timeline' => [
                'variables' => [

                    'defaut' => [
                        'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                        'regroupement' => true,
                        'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                        'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                        'nb_ligne' => 1000
                    ],
                    'accueil' => [
                        'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                        'regroupement' => true,
                        'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                        'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                        'nb_ligne' => 50
                    ],
                    'historique' => [
                        'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                        'regroupement' => true,
                        'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                        'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                        'nb_ligne' => 0
                    ],
                ]

            ],
        ];


    }




}





