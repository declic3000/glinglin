<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariableInterface;

class VariablesApp extends VariableInterface
{
    function getVariables()
    {
        return [
            'options' => [
                'prefixe_table' => '',
                'cle_id' => true,
                'suffixe_table_avec_s' => false,
                'template_sous_dossier' => true,
                'utilise_droit' => false,
                'utilise_entite' => false,
                'systeme_pref' => 'table',
                'ged' => [
                    'fichier_ou_base' => true
                ],
                'log' => [
                    'utilise_liens' => false,
                    'code' => []
                ],
                'tache' => [
                    'robot' => true,
                    'init' => [
                        'rapport' => ['Rapport périodique d\'activité', 'P1M'],
                        // 'zone'=>['Zonage des individu','PT10S']
                    ]
                ],
                'table_sys' => [
                    "config" => [
                        "table_sql" => 'configs',
                        "cle" => 'id_config',
                        "col_name" => 'nom'
                    ],
                    "preference" => [],
                    "utilisateur" => []
                ]
            ],
            'objets' => [
                'categorie'=>[
                    'cle' => 'id'
                ],
                'contact'=>[
                    'cle' => 'id'
                ],
                'lieu'=>[
                    'cle' => 'id'
                ],
                'caracteristique'=>[
                    'cle' => 'id'
                ],
                'rgpd_question'=>[
                    'cle' => 'id'
                ]
            ],
            'liens_role_objet' => [
                'A' => ['admin' => 'all', 'generique' => 'all', 'com' => 'all', 'geo' => 'all', 'doc' => 'all'],
                'G' => ['generique' => 'all']
            ]

        ];
    }
}