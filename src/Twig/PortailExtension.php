<?php
namespace App\Twig;

use App\Component\Menu;
use App\Service\CookieService;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Requete;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Declic3000\Pelican\Service\Gendarme;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Twig_SimpleFilter;

class PortailExtension extends AbstractExtension
{

    protected $sac;

    protected $router;

    protected $translator;

    protected $twig;

    protected $suc;

    protected $csrf;

    protected $gendarme;

    protected $eventDispatcher;
    protected $requete;
    protected $cookieService;

    function __construct(Requete $requete,Sac $sac, Suc $suc, UrlGeneratorInterface $router, TranslatorInterface $translator, Environment $twig, Gendarme $gendarme, CsrfTokenManagerInterface $csrf,EventDispatcherInterface $eventDispatcher,CookieService $cookieService)
    {
        $this->requete = $requete;
        $this->sac = $sac;
        $this->router = $router;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->suc = $suc;
        $this->csrf = $csrf;
        $this->gendarme = $gendarme;
        $this->eventDispatcher = $eventDispatcher;
        $this->cookieService = $cookieService;

    }


    public function getFilters(): array
    {
        return parent::getFilters()+[
            new TwigFilter('carte_adherent_etat', function ($tab_id_mot) {
                $etat = 0;
                if (! empty($tab_id_mot)) {
                    $mot_groupe = table_filtrer_valeur_premiere($this->sac->tab('motgroupe'), 'nomcourt', 'carte');
                    $mots = table_filtrer_valeur($this->sac->tab('mot'), 'id_motgroupe', $mot_groupe['id_motgroupe']);

                    foreach ($mots as $id => $m) {
                        if (in_array($id, $tab_id_mot)) {
                            $etat = max($etat, substr((string) $m['nomcourt'], - 1)+0);
                        }
                    }
                }

                return $etat;
            }),
                new TwigFilter('trafiquer', fn($truc) => substr((string) $truc,0,3).'....'.substr((string) $truc,-3)),
                new TwigFilter('urlify', fn($texte) => $this->urlify($texte))

        ];
    }



    public function getFunctions(): array
    {
        return [
            new TwigFunction('genererCalendrier', fn($tab_evts, $date) => $this->generer_calendrier($tab_evts,$date)),
            new TwigFunction('testParticipation', function ($evt,$identite) {
                $id = strtolower(trim($identite['nom']).':'.trim($identite['email']));
                return in_array($id,$evt->getParticipantsTab());
            }),
            new TwigFunction('menu_general', function () {
                $menu = new Menu($this->sac,$this->eventDispatcher );
                return $menu->get();
            }),
            new TwigFunction('identite', function () {

                $value = $this->cookieService->getIdentite();
                $args=['ok'=>false];
                if (!empty($value)){
                    $args=$value;
                    $args['ok']=true;
                }
                return $args;
            }),


        ];
    }

    function urlify($texte)
    {
        $url = '@http(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.\<]+[^\s\<]*)+[^,.\s\<])@iu';
        return preg_replace($url, '<a href="http$1://$3" target="_blank" title="$0">$0</a>', (string) $texte);
    }



    function generer_calendrier($tab_evts, $date, $day_name_length = 3, $month_href = NULL, $first_day = 0, $pn = [])
    {

        $year = $date->format('Y');
        $month = $date->format('n');
        $days = $date->format('d');
        return $this->twig->render('calendrier/calendrier.html.twig',[]);


        $first_of_month = gmmktime(0, 0, 0, $month, 1, $year);
        $day_names = [];
        for ($n = 0, $t = (3 + $first_day) * 86400; $n < 7; $n++, $t+=86400) {
            $day_names[$n] = ucfirst(gmstrftime('%A', $t));
        }
        [$month, $year, $month_name, $weekday] = explode(',', gmstrftime('%m, %Y, %B, %w', $first_of_month));
        $weekday = ($weekday + 7 - $first_day) % 7;
        $title   = htmlentities(ucfirst((string) $month_name)) . $year;

        $p ='&laquo;';
        $pl = $month - 1;
        $n = '&raquo;';
        $nl = $month + 1;

        if($p) $p = '<span class="calendar-prev">' . ($pl ? '<a href="' . htmlspecialchars((string) $pl) . '">' . $p . '</a>' : $p) . '</span>&nbsp;';
        if($n) $n = '&nbsp;<span class="calendar-next">' . ($nl ? '<a href="' . htmlspecialchars((string) $nl) . '">' . $n . '</a>' : $n) . '</span>';

        $calendar = "<div class=\"mini_calendar\"><table>".
            '<caption class="calendar-month">' . $p . ($month_href ? '<a href="' . htmlspecialchars((string) $month_href) . '">' . $title . '</a>' : $title) . $n . "</caption>\n<tr>";
        if($day_name_length)
        {
            foreach($day_names as $d)
                $calendar  .= '<th abbr="' . htmlentities((string) $d) . '">' . htmlentities($day_name_length < 4 ? substr((string) $d,0,$day_name_length) : $d) . '</th>';
            $calendar  .= "</tr><tr>";
        }

        if($weekday > 0)
        {
            for ($i = 0; $i < $weekday; $i++)
            {
                $calendar  .= '<td>&nbsp;</td>';
            }
        }
        for($day = 1, $days_in_month = gmdate('t',$first_of_month); $day <= $days_in_month; $day++, $weekday++)
        {
            if($weekday == 7)
            {
                $weekday   = 0;
                $calendar  .= "</tr><tr>";
            }
            if(isset($days[$day]) and is_array($days[$day]))
            {
                @[$link, $classes, $content] = $days[$day];
                if(is_null($content))  $content  = $day;
                $calendar  .= '<td' . ($classes ? ' class="' . htmlspecialchars((string) $classes) . '">' : '>') .
                    ($link ? '<a href="' . htmlspecialchars((string) $link) . '">' . $content . '</a>' : $content) . '</td>';
            }
            else $calendar  .= "<td>$day</td>";
        }
        if($weekday != 7) $calendar  .= '<td id="emptydays" colspan="' . (7-$weekday) . '">&nbsp;</td>'; //remaining "empty" days

        return $calendar . "</tr></table></div>";
    }
}



