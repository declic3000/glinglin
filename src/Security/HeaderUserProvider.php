<?php
namespace App\Security;

use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class HeaderUserProvider implements UserProviderInterface
{
    public function loadUserByIdentifier($identifier): UserInterface
    {
        if ($identifier) {
            $password = "dummy";
            $salt = "";
            $roles = ['ROLE_USER'];
            return new HeaderUser($identifier, $password, $salt, $roles);
        }
        throw new UserNotFoundException();
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof HeaderUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', $user::class)
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class): bool
    {
        return HeaderUser::class === $class;
    }
}