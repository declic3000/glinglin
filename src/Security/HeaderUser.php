<?php
namespace App\Security;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class HeaderUser implements UserInterface, EquatableInterface
{
    public function __construct(private $username, private $password, private $salt, private readonly array $roles)
    {
    }

    public function getRoles() : array
    {
        return $this->roles;
    }

    public function getPassword() :string
    {
        return $this->password;
    }

    public function getSalt() :string
    {
        return $this->salt;
    }
    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function eraseCredentials(): void
    {
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof HeaderUser) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        //if ($this->password !== $user->getPassword()) {
        //    return false;
        //}

        return true;
    }
}